-- Exercise 9 - RI across databases

CREATE DATABASE dbStuk
CREATE DATABASE dbBezettingsregel
GO

USE dbStuk

create table Componist (
   componistId          numeric(4)           not null,
   naam                 varchar(20)          not null,
   geboortedatum        datetime             null,
   schoolId             numeric(2)           null,
   constraint PK_COMPONIST primary key  (componistId),
   constraint AK_COMPONIST unique (naam)
)
go

create table Genre (
   genrenaam            varchar(10)          not null,
   constraint PK_GENRE primary key  (genrenaam)
)
go

create table Muziekschool (
   schoolId             numeric(2)           not null,
   naam                 varchar(30)          not null,
   plaatsnaam           varchar(20)          not null,
   constraint PK_MUZIEKSCHOOL primary key  (schoolId),
   constraint AK_MUZIEKSCHOOL unique (naam)
)
go

create table Niveau (
   niveaucode           char(1)              not null,
   omschrijving         varchar(15)          not null,
   constraint PK_NIVEAU primary key  (niveaucode),
   constraint AK_NIVEAU unique (omschrijving)
)
go

create table Stuk (
   stuknr               numeric(5)           not null,
   componistId          numeric(4)           not null,
   titel                varchar(20)          not null,
   stuknrOrigineel      numeric(5)           null,
   genrenaam            varchar(10)          not null,
   niveaucode           char(1)              null,
   speelduur            numeric(3,1)         null,
   jaartal              numeric(4)           not null,
   constraint PK_STUK primary key  (stuknr),
   constraint AK_STUK unique (componistId, titel)
)
go

USE dbBezettingsregel

create table Bezettingsregel (
   stuknr               numeric(5)           not null,
   instrumentnaam       varchar(14)          not null,
   toonhoogte           varchar(7)           not null,
   aantal               numeric(2)           not null,
   constraint PK_BEZETTINGSREGEL primary key  (stuknr, instrumentnaam, toonhoogte)
)
go

create table Instrument (
   instrumentnaam       varchar(14)          not null,
   toonhoogte           varchar(7)           not null,
   constraint PK_INSTRUMENT primary key  (instrumentnaam, toonhoogte)
)
go
