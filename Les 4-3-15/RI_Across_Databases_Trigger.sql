USE dbStuk
GO

ALTER TRIGGER trgStuk
ON Stuk
AFTER DELETE
AS BEGIN
	IF (EXISTS (SELECT * FROM dbBezettingsregel.dbo.Bezettingsregel b
		INNER JOIN deleted d ON b.stuknr = d.stuknr))
	BEGIN
		RAISERROR('Het stuk mag niet verwijderd worden', 16, 1)
		ROLLBACK TRAN
	END
END
GO

USE dbStuk
GO
INSERT INTO Stuk VALUES ( 1,  1, 'Blue bird',       NULL, 'jazz',     NULL, 4.5,  1954);
INSERT INTO Stuk VALUES ( 2,  2, 'Blue bird',       1,    'jazz',     'B',  4,    1988);
INSERT INTO Stuk VALUES ( 3,  4, 'Air pur charmer', NULL, 'klassiek', 'B',  4.5,  1953);
INSERT INTO Stuk VALUES ( 5,  5, 'Lina',            NULL, 'klassiek', 'B',  5,    1979);
INSERT INTO Stuk VALUES ( 8,  8, 'Berceuse',        NULL, 'klassiek', NULL, 4,    1786);
INSERT INTO Stuk VALUES ( 9,  2, 'Cradle song',     8,    'klassiek', 'B',  3.5,  1990);
INSERT INTO Stuk VALUES (10,  8, 'Non piu andrai',  NULL, 'klassiek', NULL, NULL, 1791);
INSERT INTO Stuk VALUES (12,  9, 'I''ll never go',  10,   'pop',      'A',  6,    1996);
INSERT INTO Stuk VALUES (13, 10, 'Swinging Lina',   5,    'jazz',     'B',  8,    1997);
INSERT INTO Stuk VALUES (14,  5, 'Little Lina',     5,    'klassiek', 'A',  4.3,  1998);
INSERT INTO Stuk VALUES (15, 10, 'Blue sky',        1,    'jazz',     'A',  4,    1998);

USE dbBezettingsregel
GO
INSERT INTO Bezettingsregel VALUES ( 2, 'drums',    '',      1);
INSERT INTO Bezettingsregel VALUES ( 2, 'saxofoon', 'alt',   2);
INSERT INTO Bezettingsregel VALUES ( 2, 'saxofoon', 'tenor', 1);
INSERT INTO Bezettingsregel VALUES ( 2, 'piano',    '',      1);
INSERT INTO Bezettingsregel VALUES ( 3, 'fluit',    '',      1);
INSERT INTO Bezettingsregel VALUES ( 5, 'fluit',    '',      3);
INSERT INTO Bezettingsregel VALUES ( 9, 'fluit',    '',      1);
INSERT INTO Bezettingsregel VALUES ( 9, 'fluit',    'alt',   1);
INSERT INTO Bezettingsregel VALUES ( 9, 'piano',    '',      1);
INSERT INTO Bezettingsregel VALUES (12, 'piano',    '',      1);
INSERT INTO Bezettingsregel VALUES (12, 'fluit',    '',      2);
INSERT INTO Bezettingsregel VALUES (13, 'drums',    '',      1);
INSERT INTO Bezettingsregel VALUES (13, 'saxofoon', 'alt',   1);
INSERT INTO Bezettingsregel VALUES (13, 'saxofoon', 'tenor', 1);
INSERT INTO Bezettingsregel VALUES (13, 'fluit',    '',      2);
INSERT INTO Bezettingsregel VALUES (14, 'piano',    '',      1);
INSERT INTO Bezettingsregel VALUES (14, 'fluit',    '',      1);
INSERT INTO Bezettingsregel VALUES (15, 'saxofoon', 'alt',   2);
INSERT INTO Bezettingsregel VALUES (15, 'fluit',    'alt',   2);
INSERT INTO Bezettingsregel VALUES (15, 'piano',    '',      1);




BEGIN TRAN
DELETE FROM dbStuk.dbo.Stuk WHERE stuknr = 2
ROLLBACK TRAN

SELECT * FROM dbStuk.dbo.Stuk WHERE stuknr = 2

-- als er geen bezetting is
BEGIN TRAN
SELECT * FROM dbStuk.dbo.Stuk WHERE stuknr = 2
DELETE FROM dbBezettingsregel.dbo.Bezettingsregel WHERE stuknr = 2
DELETE FROM dbStuk.dbo.Stuk WHERE stuknr = 2
SELECT * FROM dbStuk.dbo.Stuk WHERE stuknr = 2
ROLLBACK TRAN