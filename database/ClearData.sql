SET NOCOUNT ON
DECLARE @i INT = 0
WHILE (@i < 5)
BEGIN
	SET @i += 1;
	DELETE FROM Balie
	DELETE FROM Gate
	DELETE FROM Maatschappij
	DELETE FROM IncheckenBijMaatschappij
	DELETE FROM Luchthaven
	DELETE FROM IncheckenVoorBestemming
	DELETE FROM Vliegtuig
	DELETE FROM Vlucht
	DELETE FROM IncheckenVoorVlucht
	DELETE FROM Passagier
	DELETE FROM PassagierVoorVlucht
	DELETE FROM Object
END