CREATE PROCEDURE sp_insertMaatschappij (
	@maatschappijcode CHAR(2),
	@naam VARCHAR(255),
	@balienummer INTEGER
) AS BEGIN
	DECLARE @tranCounter INT = @@TRANCOUNT
	IF @tranCounter > 0
		SAVE TRANSACTION insertMaatschappij
	ELSE 
		BEGIN TRANSACTION
	BEGIN TRY
		--check of balie wel bestaat
		IF NOT EXISTS (SELECT * FROM Balie WHERE balienummer = @balienummer)
			RAISERROR('De opgegeven balie bestaat niet', 16, 1)
		--check of combinatie maatschappij en balie al bestaat
		IF EXISTS (SELECT * FROM IncheckenBijMaatschappij 
					WHERE maatschappijcode = @maatschappijcode
					AND balienummer = @balienummer)
			RAISERROR('Deze combinatie balienummer en maatschappijcode bestaat al', 16, 1)

		IF NOT EXISTS (SELECT * FROM Maatschappij WHERE maatschappijcode = @maatschappijcode)
			INSERT INTO Maatschappij (maatschappijcode, naam) VALUES (@maatschappijcode, @naam)

		INSERT INTO IncheckenBijMaatschappij (balienummer, maatschappijcode) VALUES (@balienummer, @maatschappijcode)

		IF @tranCounter = 0
			COMMIT TRANSACTION
	END TRY
	BEGIN CATCH
		IF @tranCounter = 0
			ROLLBACK TRANSACTION
		ELSE 
			ROLLBACK TRANSACTION insertMaatschappij

		DECLARE @ErrorMessage NVARCHAR(4000);
        DECLARE @ErrorSeverity INT;
        DECLARE @ErrorState INT;

        SELECT @ErrorMessage = ERROR_MESSAGE();
        SELECT @ErrorSeverity = ERROR_SEVERITY();
        SELECT @ErrorState = ERROR_STATE();

        RAISERROR (@ErrorMessage, @ErrorSeverity, @ErrorState);
	END CATCH
END

GO

CREATE TRIGGER trg_deleteBalieFromMaatschappij
ON IncheckenBijMaatschappij
AFTER DELETE
AS BEGIN
	DECLARE @rowCounter	INT = @@ROWCOUNT
	IF @rowCounter = 0
		RETURN

	BEGIN TRY
		IF NOT EXISTS (SELECT 1 FROM IncheckenBijMaatschappij ibm
				INNER JOIN deleted d ON d.maatschappijcode = ibm.maatschappijcode)
			RAISERROR('Er moet minstens een balie voor deze maatschappij bestaan.', 16, 1);
	END TRY
	BEGIN CATCH
		ROLLBACK TRANSACTION
        
		DECLARE @ErrorMessage NVARCHAR(4000);
        DECLARE @ErrorSeverity INT;
        DECLARE @ErrorState INT;

        SELECT @ErrorMessage = ERROR_MESSAGE();
        SELECT @ErrorSeverity = ERROR_SEVERITY();
        SELECT @ErrorState = ERROR_STATE();

        RAISERROR (@ErrorMessage, @ErrorSeverity, @ErrorState);
	END CATCH
END 