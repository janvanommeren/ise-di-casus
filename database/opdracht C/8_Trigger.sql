CREATE TRIGGER tgr_balieVoorVlucht
ON IncheckenVoorVlucht
AFTER INSERT, UPDATE
AS BEGIN
	IF @@ROWCOUNT = 0
		RETURN
	BEGIN TRY
		IF EXISTS (SELECT i.balienummer, i.vluchtnummer FROM inserted i EXCEPT (
				SELECT ibm.balienummer, v.vluchtnummer FROM vlucht v
				INNER JOIN IncheckenBijMaatschappij ibm ON ibm.maatschappijcode = v.maatschappijcode
				INNER JOIN IncheckenVoorBestemming ivb ON ivb.balienummer = ibm.balienummer 
					AND ivb.luchthavencode = v.luchthavencode)) 
			RAISERROR('Vanaf deze balie mag voor de desbetreffende vlucht of bestemming daar niet ingecheckt worden.', 16, 1)
	END TRY
	BEGIN CATCH
		ROLLBACK TRANSACTION

		DECLARE @ErrorMessage NVARCHAR(4000);
        DECLARE @ErrorSeverity INT;
        DECLARE @ErrorState INT;

        SELECT @ErrorMessage = ERROR_MESSAGE();
        SELECT @ErrorSeverity = ERROR_SEVERITY();
        SELECT @ErrorState = ERROR_STATE();

        RAISERROR (@ErrorMessage, @ErrorSeverity, @ErrorState);
	END CATCH
END
