CREATE FUNCTION dbo.fnc_PassagierBagageVlucht (
	@vluchtnummer INT,
	@passagiernummer INT
) RETURNS BIT
BEGIN
	IF ((SELECT v.max_ppgewicht
		FROM Vlucht v
		WHERE v.vluchtnummer = @vluchtnummer)
		>= 
		(SELECT SUM(o.gewicht)
		FROM Object o
		WHERE passagiernummer = @passagiernummer 
		AND vluchtnummer = @vluchtnummer
		))
		RETURN 1

	RETURN 0
END
GO

ALTER TABLE [Object]
	ADD CONSTRAINT chk_PassagierBagageVlucht
	CHECK (dbo.fnc_PassagierBagageVlucht(vluchtnummer, passagiernummer) = 1)
