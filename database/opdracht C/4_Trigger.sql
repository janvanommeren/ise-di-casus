CREATE TRIGGER trg_maxPassagiersVoorVlucht 
ON PassagierVoorVlucht AFTER INSERT
AS BEGIN
	IF @@ROWCOUNT = 0
		RETURN
	BEGIN TRY
		IF EXISTS(SELECT * FROM PassagierVoorVlucht pvv
			INNER JOIN Vlucht v ON pvv.vluchtnummer = v.vluchtnummer
			GROUP BY v.max_aantal_psgrs
			HAVING COUNT(*) > v.max_aantal_psgrs)
		BEGIN
			RAISERROR('Het max aantal passagiers voor de betreffende vlucht is overschreden.', 16, 1)
		END
	END TRY
	BEGIN CATCH
		ROLLBACK TRANSACTION

		DECLARE @ErrorMessage NVARCHAR(4000);
        DECLARE @ErrorSeverity INT;
        DECLARE @ErrorState INT;

        SELECT @ErrorMessage = ERROR_MESSAGE();
        SELECT @ErrorSeverity = ERROR_SEVERITY();
        SELECT @ErrorState = ERROR_STATE();

        RAISERROR (@ErrorMessage, @ErrorSeverity, @ErrorState);
	END CATCH
END
GO