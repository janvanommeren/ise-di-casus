-- Verander de colommen in NOT NULL
ALTER TABLE Vlucht
	ALTER COLUMN vertrektijdstip DATETIME NOT NULL

ALTER TABLE Vlucht
	ALTER COLUMN aankomsttijdstip DATETIME NOT NULL
GO

CREATE FUNCTION dbo.fnc_passagierOverlappendeVlucht (
	@vluchtnummer INT,
	@passagiernummer INT
) RETURNS BIT
BEGIN
	IF (SELECT count(1) FROM PassagierVoorVlucht WHERE passagiernummer = @passagiernummer ) = 1
		RETURN 1
	
	IF EXISTS (
		SELECT pvv.passagiernummer, pvv.vluchtnummer, v.vertrektijdstip, v.aankomsttijdstip
		FROM PassagierVoorVlucht pvv
		INNER JOIN Vlucht v ON pvv.vluchtnummer = v.vluchtnummer
		WHERE pvv.passagiernummer = @passagiernummer AND v.vluchtnummer = @vluchtnummer AND
		pvv.passagiernummer IN ( 
			SELECT passagiernummer
			FROM Vlucht v2
			WHERE (v.vertrektijdstip > v2.vertrektijdstip AND v.vertrektijdstip < v2.aankomsttijdstip )
				OR (v.vertrektijdstip < v2.vertrektijdstip AND v.aankomsttijdstip > v2.aankomsttijdstip)
		)
	)
		RETURN 0

	RETURN 1
END
GO

ALTER TABLE PassagierVoorVlucht
	ADD CONSTRAINT chk_passagierOverlappendeVlucht CHECK (dbo.fnc_passagierOverlappendeVlucht(vluchtnummer, passagiernummer) = 1)
GO
