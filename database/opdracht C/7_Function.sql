CREATE FUNCTION dbo.fnc_vluchtStoelNummerIsUnique (
	@vluchtnummer INTEGER,
	@stoel CHAR(3)
) RETURNS BIT
BEGIN
	IF @stoel IS NULL OR (SELECT COUNT(*) FROM PassagierVoorVlucht 
		WHERE vluchtnummer = @vluchtnummer
		AND stoel = @stoel) <= 1 
		RETURN 1

	RETURN 0
END

GO

ALTER TABLE PassagierVoorVlucht 
	ADD CONSTRAINT chk_VluchtStoelNummer
	CHECK (dbo.fnc_vluchtStoelNummerIsUnique(vluchtnummer, stoel) = 1)