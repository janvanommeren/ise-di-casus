-- Voor elke passagier zijn het stoelnummer en het inchecktijdstip �f beide niet ingevuld �f beide wel ingevuld.
ALTER TABLE PassagierVoorVlucht
	ADD CONSTRAINT chk_stoel_inchecktijdstip CHECK (
		(inchecktijdstip IS NULL AND stoel IS NULL) 
		OR (
			inchecktijdstip IS NOT NULL AND stoel IS NOT NULL
		)
	)