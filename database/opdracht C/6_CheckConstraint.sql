
ALTER TABLE Vlucht
ADD CONSTRAINT chk_maxGewichtPerPersoon CHECK (
	(max_aantal_psgrs*max_ppgewicht <= max_totaalgewicht)
)