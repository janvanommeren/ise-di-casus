CREATE FUNCTION dbo.fnc_checkIncheckTijd (
	@vluchtnummer INT,
	@incheckTijdstip DATETIME
) RETURNS BIT
BEGIN
	IF @incheckTijdstip IS NULL
		RETURN 1

	IF (@incheckTijdstip >= (SELECT vertrektijdstip FROM Vlucht WHERE vluchtnummer = @vluchtnummer))
		RETURN 0

	RETURN 1
END
GO

ALTER TABLE PassagierVoorVlucht 
	ADD CONSTRAINT chk_checkIncheckTijd
		CHECK (dbo.fnc_checkIncheckTijd(vluchtnummer, inchecktijdstip) = 1)