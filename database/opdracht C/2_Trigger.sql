--	Als er een passagier aan een vlucht is toegevoegd mogen de gegevens van die vlucht niet meer gewijzigd worden. 	

CREATE TRIGGER trg_passagierInVlucht
ON Vlucht
AFTER UPDATE
AS BEGIN
	DECLARE @aantalPassagiers	INT,
			@rowCounter			INT = @@ROWCOUNT

	IF @rowCounter = 0
		RETURN

	BEGIN TRY
		IF EXISTS (
		SELECT d.vluchtnummer
		FROM deleted d
			INNER JOIN PassagierVoorVlucht pvv ON d.vluchtnummer = pvv.vluchtnummer )
			RAISERROR('Er zijn al passagiers ingecheckt voor deze vlucht. De vlucht mag niet meer gewijzigt worden.', 16, 1)
	END TRY
	BEGIN CATCH
		ROLLBACK TRANSACTION
        
		DECLARE @ErrorMessage NVARCHAR(4000);
        DECLARE @ErrorSeverity INT;
        DECLARE @ErrorState INT;

        SELECT @ErrorMessage = ERROR_MESSAGE();
        SELECT @ErrorSeverity = ERROR_SEVERITY();
        SELECT @ErrorState = ERROR_STATE();

        RAISERROR (@ErrorMessage, @ErrorSeverity, @ErrorState);
	END CATCH
END 
