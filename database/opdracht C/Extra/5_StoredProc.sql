CREATE PROCEDURE insertBagage (
	@volgnummer INT,
	@passagiernummer INT,
	@vluchtnummer INT,
	@gewicht INT
) AS BEGIN
	DECLARE @tranCounter INT = @@TRANCOUNT
	IF @tranCounter > 0
		SAVE TRANSACTION insertBagage
	ELSE 
		BEGIN TRANSACTION
	BEGIN TRY
		IF (SELECT SUM(gewicht) 
			FROM Object 
			WHERE passagiernummer = @passagiernummer 
			AND vluchtnummer = @vluchtnummer) + @gewicht > (SELECT max_ppgewicht 
															FROM Vlucht 
															WHERE vluchtnummer = @vluchtnummer)
			RAISERROR('Het maximale gewicht per persoon voor deze vlucht is overschreden', 1, 16);

		INSERT INTO Object (volgnummer, passagiernummer, vluchtnummer, gewicht)
			VALUES (@volgnummer, @passagiernummer, @vluchtnummer, @gewicht)

		IF @tranCounter = 0
			COMMIT TRANSACTION
	END TRY
	BEGIN CATCH
		IF @tranCounter = 0
			ROLLBACK TRANSACTION
		ELSE 
			ROLLBACK insertBagage

		DECLARE @ErrorMessage NVARCHAR(4000);
        DECLARE @ErrorSeverity INT;
        DECLARE @ErrorState INT;

        SELECT @ErrorMessage = ERROR_MESSAGE();
        SELECT @ErrorSeverity = ERROR_SEVERITY();
        SELECT @ErrorState = ERROR_STATE();

        RAISERROR (@ErrorMessage, @ErrorSeverity, @ErrorState);
	END CATCH
END