--Het totaalgewicht van de bagage van een passagier mag het maximaal per persoon toegestane gewicht op een vlucht niet overschrijden.


--Dit is een CHECK met een functie op Object die bekijkt of er meerdere objecten bij de passagier behoren, 
--zo ja dan worden deze opgeteld en wordt er volgens gekeken of het max_ppgewicht van de bijbehorende vlucht groter is dan het totaal gewicht van de objecten.

ALTER TRIGGER tgr_maxGewichtPerPersoon
ON [Object]
AFTER INSERT
AS BEGIN
	IF @@ROWCOUNT = 0
		RETURN
	BEGIN TRY
		IF EXISTS(
			SELECT SUM(sub.gewicht)
			FROM 
			(
				SELECT o.gewicht, v.max_ppgewicht
				FROM [Object] o
				INNER JOIN inserted i ON o.passagiernummer = i.passagiernummer AND o.vluchtnummer = i.vluchtnummer
				INNER JOIN Vlucht v ON o.vluchtnummer = v.vluchtnummer
				GROUP BY o.volgnummer, v.max_ppgewicht, o.gewicht, o.passagiernummer
			) as sub
			HAVING SUM(sub.gewicht) > SUM(sub.max_ppgewicht)/COUNT(sub.max_ppgewicht)
		)
			RAISERROR('De bagage overschijd het maximaal toegestane gewicht.', 16, 1)
	END TRY
	BEGIN CATCH
		ROLLBACK TRANSACTION

		DECLARE @ErrorMessage NVARCHAR(4000);
        DECLARE @ErrorSeverity INT;
        DECLARE @ErrorState INT;

        SELECT @ErrorMessage = ERROR_MESSAGE();
        SELECT @ErrorSeverity = ERROR_SEVERITY();
        SELECT @ErrorState = ERROR_STATE();

        RAISERROR (@ErrorMessage, @ErrorSeverity, @ErrorState);
	END CATCH
END

GO

