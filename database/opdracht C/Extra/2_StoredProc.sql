CREATE PROCEDURE updateVlucht (
	@vluchtnummer INT,
	@gatecode CHAR(1),
	@maatschappijcode CHAR(2),
	@luchthavencode CHAR(3),
	@vliegtuigtype VARCHAR(30),
	@max_aantal_psgrs INT,
	@max_totaalgewicht NUMERIC(5,0),
	@max_ppgewicht NUMERIC(5,2),
	@vertrektijdstip DATETIME,
	@aankomsttijdstip DATETIME
) AS BEGIN
	DECLARE @tranCounter INT = @@TRANCOUNT
	IF @tranCounter > 0
		SAVE TRANSACTION insertBagage
	ELSE 
		BEGIN TRANSACTION
	BEGIN TRY
		IF EXISTS (SELECT 1 FROM PassagierVoorVlucht WHERE vluchtnummer = @vluchtnummer)
			RAISERROR('De gegevens van de vlucht mogen niet meer gewijzigd worden.', 1, 16);
		
		UPDATE Vlucht SET gatecode = @gatecode, maatschappijcode = @maatschappijcode, 
			luchthavencode = @luchthavencode, vliegtuigtype = @vliegtuigtype, 
			max_aantal_psgrs = @max_aantal_psgrs, max_totaalgewicht = @max_totaalgewicht, 
			max_ppgewicht = @max_ppgewicht, vertrektijdstip = @vertrektijdstip, 
			aankomsttijdstip = @aankomsttijdstip WHERE vluchtnummer = @vluchtnummer

		IF @tranCounter = 0
			COMMIT TRANSACTION
	END TRY
	BEGIN CATCH
		IF @tranCounter = 0
			ROLLBACK TRANSACTION
		ELSE 
			ROLLBACK insertBagage

		DECLARE @ErrorMessage NVARCHAR(4000);
        DECLARE @ErrorSeverity INT;
        DECLARE @ErrorState INT;

        SELECT @ErrorMessage = ERROR_MESSAGE();
        SELECT @ErrorSeverity = ERROR_SEVERITY();
        SELECT @ErrorState = ERROR_STATE();

        RAISERROR (@ErrorMessage, @ErrorSeverity, @ErrorState);
	END CATCH
END