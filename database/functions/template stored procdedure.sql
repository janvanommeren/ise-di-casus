CREATE PROCEDURE procTemplate
AS BEGIN
	DECLARE @tranCounter INT = @@TRANCOUNT
	IF @tranCounter > 0
		SAVE TRANSACTION procTemplateSave
	ELSE 
		BEGIN TRANSACTION
	BEGIN TRY
		-- do something
		SELECT * FROM checkings
		-- if error -> RAISEERROR('message', 16, 0);

		IF @tranCounter = 0
			COMMIT TRANSACTION
	END TRY
	BEGIN CATCH
		IF @tranCounter = 0
			ROLLBACK TRANSACTION
		ELSE 
			ROLLBACK procTemplateSave

		DECLARE @ErrorMessage NVARCHAR(4000);
        DECLARE @ErrorSeverity INT;
        DECLARE @ErrorState INT;

        SELECT @ErrorMessage = ERROR_MESSAGE();
        SELECT @ErrorSeverity = ERROR_SEVERITY();
        SELECT @ErrorState = ERROR_STATE();

        RAISERROR (@ErrorMessage, @ErrorSeverity, @ErrorState);
	END CATCH
END