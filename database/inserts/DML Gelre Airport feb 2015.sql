
/* INSERT statements Gelre Airport.
   Dit zijn er maar enkele om je op weg te helpen. Je mag ook 
   je eigen populatie aanmaken.
 */
USE gelre_airport
go

INSERT Balie(balienummer)
  VALUES (1)
INSERT Balie(balienummer)
  VALUES (2)
INSERT Balie(balienummer)
  VALUES (3)
INSERT Balie(balienummer)
  VALUES (4)
INSERT Balie(balienummer)
  VALUES (5)


INSERT Luchthaven(luchthavencode,naam,land)
  VALUES ( 'DUB','Dublin Airport','Ierland');
INSERT Luchthaven(luchthavencode,naam,land)
  VALUES ( 'BRI','British Airport','Engeland');
INSERT Luchthaven(luchthavencode,naam,land)
  VALUES ( 'FRE','French Airport','Frankrijk');
INSERT Luchthaven(luchthavencode,naam,land)
  VALUES ( 'UAL','United Airport','VS');
INSERT Luchthaven(luchthavencode,naam,land)
  VALUES ( 'NZA','New Zealand Airport','New Zealand');


INSERT Gate(gatecode)
  VALUES ( 'C');
INSERT Gate(gatecode)
  VALUES ( 'D');
INSERT Gate(gatecode)
  VALUES ( 'E');
INSERT Gate(gatecode)
  VALUES ( 'A');
INSERT Gate(gatecode)
  VALUES ( 'B');
INSERT Gate(gatecode)
  VALUES ( 'F');


INSERT Maatschappij(maatschappijcode,naam)
  VALUES ( 'KL','KLM');
INSERT Maatschappij(maatschappijcode,naam)
  VALUES ( 'BA','BRITISH AIRWAYS');
INSERT Maatschappij(maatschappijcode,naam)
  VALUES ( 'AF','AIR FRANCE');
INSERT Maatschappij(maatschappijcode,naam)
  VALUES ( 'UA','UNITED AIRLINES');
INSERT Maatschappij(maatschappijcode,naam)
  VALUES ( 'NZ','AIR NEW ZEALAND');

INSERT Vliegtuig (vliegtuigtype) VALUES ('Boeing 747')

INSERT Vlucht(vluchtnummer,luchthavencode,vliegtuigtype,gatecode,max_aantal_psgrs,max_totaalgewicht,max_ppgewicht,vertrektijdstip,maatschappijcode)
  VALUES ( 5314,  'DUB', 'Boeing 747',  'C',  120, 2500,  20,'2004-01-30 11:30',  'KL');
INSERT Vlucht(vluchtnummer,luchthavencode,vliegtuigtype,gatecode,max_aantal_psgrs,max_totaalgewicht,max_ppgewicht,vertrektijdstip,maatschappijcode)
  VALUES ( 5315,  'DUB', 'Boeing 747',  'C',  120, 2500,  20,'2004-01-31 11:30',  'KL');
INSERT Vlucht(vluchtnummer,luchthavencode,vliegtuigtype,gatecode,max_aantal_psgrs,max_totaalgewicht,max_ppgewicht,vertrektijdstip,maatschappijcode)
  VALUES ( 5316,  'BRI', 'Boeing 747',  'D',  100, 1337,  13,'2004-02-01 13:37',  'BA');
INSERT Vlucht(vluchtnummer,luchthavencode,vliegtuigtype,gatecode,max_aantal_psgrs,max_totaalgewicht,max_ppgewicht,vertrektijdstip,maatschappijcode)
  VALUES ( 5317,  'FRE', 'Boeing 747',  'F',  133, 1900,  15,'2004-02-05 23:37',  'AF');
INSERT Vlucht(vluchtnummer,luchthavencode,vliegtuigtype,gatecode,max_aantal_psgrs,max_totaalgewicht,max_ppgewicht,vertrektijdstip,maatschappijcode)
  VALUES ( 5318,  'NZA', 'Boeing 747', 'E',  90, 1400,  15,'2004-02-07 10:08',  'NZ');


INSERT Passagier(passagiernummer,naam,geslacht,geboortedatum)
  VALUES ( 850, 'Piet', 'M', '1990-12-19');
INSERT Passagier(passagiernummer,naam,geslacht,geboortedatum)
  VALUES ( 1002, 'Jos', 'M', '1990-02-05');
INSERT Passagier(passagiernummer,naam,geslacht,geboortedatum)
  VALUES ( 1337, 'Lien','V', '1992-01-07');
INSERT Passagier(passagiernummer,naam,geslacht,geboortedatum)
  VALUES ( 1338, 'Henk', 'M', '1980-09-28');
INSERT Passagier(passagiernummer,naam,geslacht,geboortedatum)
  VALUES ( 1339, 'Bart', 'M', '2002-07-12');
INSERT Passagier(passagiernummer,naam,geslacht,geboortedatum)
  VALUES ( 1500, 'Bram', 'M', '2004-03-15');
INSERT Passagier(passagiernummer,naam,geslacht,geboortedatum)
  VALUES ( 1555, 'Geke', 'V', '1991-03-25');
INSERT Passagier(passagiernummer,naam,geslacht,geboortedatum)
  VALUES 	( 	1600, 'Bryan','M', '1987-12-31');
INSERT Passagier(passagiernummer,naam,geslacht,geboortedatum)
  VALUES 	( 	1601, 'Anne', 'V', '1977-11-23');
INSERT Passagier(passagiernummer,naam,geslacht,geboortedatum)
  VALUES 	( 	1602, 'Thomas', 'M', '1970-10-17');

INSERT PassagierVoorVlucht(passagiernummer,vluchtnummer,balienummer,stoel,inchecktijdstip)
  VALUES ( 850,  5317, 3, 75,'2004-02-05 22:25');
INSERT PassagierVoorVlucht(passagiernummer,vluchtnummer,balienummer,stoel,inchecktijdstip)
  VALUES ( 1002,  5315,  4, 1,'2004-01-31 08:30');
INSERT PassagierVoorVlucht(passagiernummer,vluchtnummer,balienummer,stoel,inchecktijdstip)
  VALUES ( 1337,  5316,  1,98,'2004-02-01 11:37');
INSERT PassagierVoorVlucht(passagiernummer,vluchtnummer,balienummer,stoel,inchecktijdstip)
  VALUES ( 1338,  5316,  1,99,'2004-02-01 11:30');
INSERT PassagierVoorVlucht(passagiernummer,vluchtnummer,balienummer,stoel,inchecktijdstip)
  VALUES ( 1339,  5316,  1,100,'2004-02-01 11:45');
INSERT PassagierVoorVlucht(passagiernummer,vluchtnummer,balienummer,stoel,inchecktijdstip)
  VALUES ( 1500,  5317,  2, 66,'2004-02-05 21:55');
INSERT PassagierVoorVlucht(passagiernummer,vluchtnummer,balienummer,stoel,inchecktijdstip)
  VALUES ( 1555,  5317,  2, 67,'2004-02-05 21:00');
INSERT PassagierVoorVlucht(passagiernummer,vluchtnummer, balienummer	, stoel ,	inchecktijdstip)
  VALUES 		( 	1600	, 5317 ,   3	, 70	,'2004-02-05 22:00');
INSERT PassagierVoorVlucht(passagiernummer,vluchtnummer, balienummer	, stoel ,	inchecktijdstip)
  VALUES 		( 	1601	, 5317 ,   3	, 71	,'2004-02-05 22:05');
INSERT PassagierVoorVlucht(passagiernummer,vluchtnummer, balienummer	, stoel, inchecktijdstip)
  VALUES 		( 	1602	, 5317 ,   3	, 72	,'2004-02-05 22:15');



INSERT 	  Object(passagiernummer, vluchtnummer, gewicht)
  VALUES		(850    , 5317,   15	);
INSERT 	  Object(passagiernummer, vluchtnummer, gewicht)
  VALUES		(850    , 5317,      1	);
INSERT 	  Object(passagiernummer, vluchtnummer, gewicht)
  VALUES		(850    , 5317,      2	);
INSERT 	  Object(passagiernummer, vluchtnummer, gewicht)
  VALUES		(850    , 5317,      2	);
INSERT 	  Object(passagiernummer, vluchtnummer, gewicht)
  VALUES		(1002    , 5315,   18	);
INSERT 	  Object(passagiernummer, vluchtnummer, gewicht)
  VALUES		(1002    , 5315,      2	);
INSERT 	  Object(passagiernummer, vluchtnummer, gewicht)
  VALUES		(1337    ,  5316, 13	);
INSERT 	  Object(passagiernummer, vluchtnummer, gewicht)
  VALUES		(1338    ,5316,    2	);
INSERT 	  Object(passagiernummer, vluchtnummer, gewicht)
  VALUES		(1338    ,5316,       10	);
INSERT 	  Object(passagiernummer, vluchtnummer, gewicht)
  VALUES		(1339    , 5316,   5	);
INSERT 	  Object(passagiernummer, vluchtnummer, gewicht)
  VALUES		(1339    , 5316,   2	);
INSERT 	  Object(passagiernummer, vluchtnummer, gewicht)
  VALUES		(1339    , 5316,   6	);
INSERT 	  Object(passagiernummer, vluchtnummer, gewicht)
  VALUES		(1500    , 5317,   8	);
INSERT 	  Object(passagiernummer, vluchtnummer, gewicht)
  VALUES		(1500    , 5317,   4	);
INSERT 	  Object(passagiernummer, vluchtnummer, gewicht)
  VALUES		(1500    , 5317,   2	);
INSERT 	  Object(passagiernummer, vluchtnummer, gewicht)
  VALUES		(1555    , 5317,   14	);
INSERT 	  Object(passagiernummer, vluchtnummer, gewicht)
  VALUES		(1600    , 5317,   2	);
INSERT 	  Object(passagiernummer, vluchtnummer, gewicht)
  VALUES		(1600    , 5317,   4	);
INSERT 	  Object(passagiernummer, vluchtnummer, gewicht)
  VALUES		(1600   , 5317,   6	);
INSERT 	  Object(passagiernummer, vluchtnummer, gewicht)
  VALUES		(1601   , 5317,   14	);

go
INSERT 	 IncheckenVoorVlucht  (vluchtnummer,balienummer)
  VALUES				 (5314 , 3);
INSERT 	 IncheckenVoorVlucht  (vluchtnummer,balienummer)
  VALUES				 (5315 , 4);
INSERT 	 IncheckenVoorVlucht  (vluchtnummer,balienummer)
  VALUES				 (5316 , 1);
INSERT 	 IncheckenVoorVlucht  (vluchtnummer,balienummer)
  VALUES				 (5317 , 2);
INSERT 	 IncheckenVoorVlucht  (vluchtnummer,balienummer)
  VALUES				 (5318 , 2);


INSERT 	  IncheckenVoorBestemming (luchthavencode,balienummer)
  VALUES				     ('DUB' 	, 3);
INSERT 	  IncheckenVoorBestemming (luchthavencode,balienummer)
  VALUES				     ('DUB' 	, 4);
INSERT 	  IncheckenVoorBestemming (luchthavencode,balienummer)
  VALUES				     ('BRI' 	, 1);
INSERT 	  IncheckenVoorBestemming (luchthavencode,balienummer)
  VALUES				     ('FRE' 	, 2);
INSERT 	  IncheckenVoorBestemming (luchthavencode,balienummer)
  VALUES				     ('NZA' 	, 2);

go

INSERT 	 IncheckenBijMaatschappij (maatschappijcode,balienummer)
  VALUES				     (	 'KL'	 , 3);
INSERT 	 IncheckenBijMaatschappij (maatschappijcode,balienummer)
  VALUES				     (	 'KL'	 , 4);
INSERT 	 IncheckenBijMaatschappij (maatschappijcode,balienummer)
  VALUES				     (	 'BA'	 , 1);
INSERT 	 IncheckenBijMaatschappij (maatschappijcode,balienummer)
  VALUES				     (	 'AF'	 , 2);
INSERT 	 IncheckenBijMaatschappij (maatschappijcode,balienummer)
  VALUES				     (	 'NZ'	 , 2);






