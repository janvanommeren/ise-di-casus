-- Testdata Constraint 9
-- Elke maatschappij moet een balie hebben.

USE gelre_airport
SET NOCOUNT ONs

BEGIN TRAN
	INSERT INTO Balie VALUES (1), (2)
	BEGIN TRY
		EXEC sp_insertMaatschappij 'JH', 'JanHarm', 1
		PRINT 'GOED - Constraint 9 - Execute StoredProcedure unieke maatschappijcode, naam en balie - inserted'
	END TRY
	BEGIN CATCH
		PRINT 'FOUT - Constraint 9 = Execute StoredProcedure unieke maatschappijcode, naam en balie - NOT inserted'
	END CATCH


	BEGIN TRY
		EXEC sp_insertMaatschappij 'JH', 'JanHarm2', 1
		PRINT 'FOUT - Constraint 9 - Execute StoredProcedure unieke maatschappijnaam - inserted'
	END TRY
	BEGIN CATCH
		PRINT 'GOED - Constraint 9 = Execute StoredProcedure unieke maatschappijnaam - NOT inserted'
	END CATCH


	BEGIN TRY
		EXEC sp_insertMaatschappij 'HJ', 'JanHarm', 1
		PRINT 'FOUT - Constraint 9 - Execute StoredProcedure unieke maatschappijcode - inserted'
	END TRY
	BEGIN CATCH
		PRINT 'GOED - Constraint 9 = Execute StoredProcedure unieke maatschappijcode - NOT inserted'
	END CATCH


	BEGIN TRY
		EXEC sp_insertMaatschappij 'JH', 'JanHarm', 2
		PRINT 'GOED - Constraint 9 - Execute StoredProcedure unieke balie - inserted'
	END TRY
	BEGIN CATCH
		PRINT 'FOUT - Constraint 9 = Execute StoredProcedure unieke balie - NOT inserted'
	END CATCH

ROLLBACK TRAN