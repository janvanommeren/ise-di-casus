-- Testdata Constraint 4
-- Elke vlucht heeft volgens de specs een toegestaan maximum aantal passagiers. Zorg ervoor dat deze regel niet overschreden kan worden.

USE gelre_airport
SET NOCOUNT ON

BEGIN TRAN

	BEGIN TRY
		INSERT INTO Gate VALUES ('A')
		INSERT INTO Maatschappij VALUES ('KL', 'KLM')
		INSERT INTO Luchthaven VALUES ('DUB', 'Dublin Airport', 'Ierland')
		INSERT INTO Vliegtuig VALUES ('Boeing 747')
		
		INSERT INTO Balie VALUES (1)
		INSERT INTO IncheckenBijMaatschappij VALUES (1, 'KL')
		INSERT INTO IncheckenVoorBestemming VALUES (1, 'DUB')
		INSERT INTO Vlucht VALUES (10, 'A', 'KL', 'DUB', 'Boeing 747', 120, 2500, 20.00, '2004-01-30 11:30:00.000', '2004-01-31 17:30:00.000')
		INSERT INTO IncheckenVoorVlucht VALUES (1, 10)		
		;
		
		PRINT 'GOED - Constraint 4 - Insert testvlucht - inserted'
	END TRY
	BEGIN CATCH 
		PRINT 'FOUT - Constraint 4 - Insert testvlucht - NOT inserted'
	END CATCH
	GO

		

	DECLARE @i INT = 0;
	BEGIN TRY
		WHILE (@i < 120)
		BEGIN
			INSERT INTO Passagier VALUES (1 + @i, 'Kel' + CAST(@i AS varchar(4)) , 'M', '1990-01-10 00:00:00.000');
			SET @i += 1;
		END
		PRINT 'GOED - Constraint 4 - Insert alle testpassagiers - inserted'
	END TRY
	BEGIN CATCH
		PRINT 'FOUT - Constraint 4 - Insert alle testpassagiers - NOT inserted'
	END CATCH


	DECLARE @j INT = 0;
	BEGIN TRY
		WHILE (@j < 120)
		BEGIN
			INSERT INTO PassagierVoorVlucht VALUES (1 + @j, 10, 1, '2004-01-29 22:25:00.000', 1 + @j);
			SET @j += 1;
		END
		PRINT 'GOED - Constraint 4 - Insert testpassagiers in testvlucht - inserted'
	END TRY
	BEGIN CATCH
		PRINT 'FOUT - Constraint 4 - Insert testpassagiers in testvlucht - NOT inserted'
	END CATCH
	
	
	BEGIN TRY
		INSERT INTO PassagierVoorVlucht VALUES (1 + @j, 10, 1, '2004-01-30 10:00:00.000', 1 + @j)
		PRINT 'FOUT - Constraint 4 - Insert laatste testpassagier, 1 teveel - inserted'
	END TRY
	BEGIN CATCH
		PRINT 'GOED - Constraint 4 - Insert laatste testpassagier, 1 teveel - NOT inserted'
	END CATCH

ROLLBACK TRAN