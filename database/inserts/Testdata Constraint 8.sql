-- Testdata Constraint 8
-- Niet alle balies zijn bruikbaar voor iedere bestemming, en niet alle balies zijn te gebruiken door iedere maatschappij. 
-- Als er balies aan een vlucht gekoppeld worden, mogen dat all��n balies zijn die toegestaan zijn voor de maatschappij en voor de bestemming van de vlucht. 
-- Er kunnen aan ��n vlucht meerdere balies gekoppeld worden. De passagier checkt uiteindelijk bij ��n van deze balies in.
USE gelre_airport
SET NOCOUNT ON

BEGIN TRAN
	BEGIN TRY
		INSERT INTO Passagier VALUES (1, 'harm', 'M', '1990-12-31 00:00:00.000')
		INSERT INTO Gate VALUES ('A')
		INSERT INTO Maatschappij VALUES ('KL', 'KLM')
		INSERT INTO Luchthaven VALUES ('DUB', 'Dublin Airport', 'Ierland')
		INSERT INTO Vliegtuig VALUES ('Boeing 747')
		INSERT INTO Vlucht VALUES (10, 'A', 'KL', 'DUB', 'Boeing 747', 120, 2500, 20.00, '2004-01-30 11:30:00.000', '2004-01-31 17:30:00.000')
		INSERT INTO Balie VALUES (1)
		INSERT INTO IncheckenBijMaatschappij VALUES (1, 'KL')
		INSERT INTO IncheckenVoorBestemming VALUES (1, 'DUB')
		INSERT INTO IncheckenVoorVlucht VALUES (1, 10)
		PRINT 'GOED - Constraint 8 - Single insert balienummer 1 - inserted'
	END TRY
	BEGIN CATCH
		PRINT 'FOUT - Constraint 8 - Single insert balienummer 1 - NOT inserted'
	END CATCH
ROLLBACK TRAN


BEGIN TRAN
	BEGIN TRY
		INSERT INTO Passagier VALUES (1, 'harm', 'M', '1990-12-31 00:00:00.000')
		INSERT INTO Gate VALUES ('A')
		INSERT INTO Maatschappij VALUES ('KL', 'KLM')
		INSERT INTO Luchthaven VALUES ('DUB', 'Dublin Airport', 'Ierland')
		INSERT INTO Vliegtuig VALUES ('Boeing 747')
		INSERT INTO Vlucht VALUES (10, 'A', 'KL', 'DUB', 'Boeing 747', 120, 2500, 20.00, '2004-01-30 11:30:00.000', '2004-01-31 17:30:00.000')
		INSERT INTO Balie VALUES (1), (2)
		INSERT INTO IncheckenBijMaatschappij VALUES (1, 'KL'), (2, 'KL')
		INSERT INTO IncheckenVoorBestemming VALUES (1, 'DUB'), (2, 'DUB')
		INSERT INTO IncheckenVoorVlucht VALUES (1, 10), (2, 10)
		PRINT 'GOED - Constraint 8 - Multi insert balienummer 1 en 2 - inserted'
	END TRY
	BEGIN CATCH
		PRINT 'FOUT - Constraint 8 - Multi insert balienummer 1 en 2- NOT inserted'
	END CATCH
ROLLBACK TRAN


BEGIN TRAN
	BEGIN TRY
		INSERT INTO Passagier VALUES (1, 'harm', 'M', '1990-12-31 00:00:00.000')
		INSERT INTO Gate VALUES ('A')
		INSERT INTO Maatschappij VALUES ('KL', 'KLM'), ('DA', 'WEZ')
		INSERT INTO Luchthaven VALUES ('DUB', 'Dublin Airport', 'Ierland'), ('WeZ', 'West East Zoef', 'Denmark')
		INSERT INTO Vliegtuig VALUES ('Boeing 747')
		INSERT INTO Vlucht VALUES (10, 'A', 'KL', 'DUB', 'Boeing 747', 120, 2500, 20.00, '2004-01-30 11:30:00.000', '2004-01-31 17:30:00.000')
		INSERT INTO Vlucht VALUES (11, 'A', 'DA', 'WEZ', 'Boeing 747', 120, 2500, 20.00, '2004-01-30 11:30:00.000', '2004-01-31 17:30:00.000')
		INSERT INTO Balie VALUES (1), (2)
		INSERT INTO IncheckenBijMaatschappij VALUES (1, 'KL'), (2, 'DA')
		INSERT INTO IncheckenVoorBestemming VALUES (1, 'DUB'), (2, 'WEZ')
		INSERT INTO IncheckenVoorVlucht VALUES (1, 10), (2, 11)
		PRINT 'GOED - Constraint 8 - Multi insert balienummer 1 en 2 voor 2 vluchten - inserted'
	END TRY
	BEGIN CATCH
		PRINT 'FOUT - Constraint 8 - Multi insert balienummer 1 en 2 voor 2 vluchten - NOT inserted'
	END CATCH
ROLLBACK TRAN