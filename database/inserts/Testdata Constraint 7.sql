-- stoel is niet ingevult, of de combinatie vlucht en stoelnummer is uniek
USE gelre_airport
SET NOCOUNT ON

BEGIN TRAN
	BEGIN TRY
		INSERT INTO Passagier VALUES (1, 'harm', 'M', '1990-12-31 00:00:00.000')
		INSERT INTO Gate VALUES ('A')
		INSERT INTO Maatschappij VALUES ('KL', 'KLM')
		INSERT INTO Luchthaven VALUES ('DUB', 'Dublin Airport', 'Ierland')
		INSERT INTO Vliegtuig VALUES ('Boeing 747')
		INSERT INTO Vlucht VALUES (10, 'A', 'KL', 'DUB', 'Boeing 747', 120, 2500, 20.00, '2004-01-30 11:30:00.000', '2004-01-31 17:30:00.000')
		INSERT INTO Balie VALUES (1)

		INSERT INTO PassagierVoorVlucht VALUES (1, 10, 1, NULL, NULL)
		PRINT 'GOED - Constraint 7 - Single insert zonder tijdstip en zonder stoel - inserted'
	END TRY
	BEGIN CATCH
		PRINT 'FOUT - Constraint 7 - Single insert zonder tijdstip en zonder stoel - NOT inserted'
	END CATCH
ROLLBACK TRAN


BEGIN TRAN
	BEGIN TRY
		INSERT INTO Passagier VALUES (1, 'harm', 'M', '1990-12-31 00:00:00.000')
		INSERT INTO Passagier VALUES (2, 'jan', 'M', '1992-01-07 00:00:00.000')
		INSERT INTO Gate VALUES ('A')
		INSERT INTO Maatschappij VALUES ('KL', 'KLM')
		INSERT INTO Luchthaven VALUES ('DUB', 'Dublin Airport', 'Ierland')
		INSERT INTO Vliegtuig VALUES ('Boeing 747')
		INSERT INTO Vlucht VALUES (10, 'A', 'KL', 'DUB', 'Boeing 747', 120, 2500, 20.00, '2004-01-30 11:30:00.000', '2004-01-31 17:30:00.000')
		INSERT INTO Balie VALUES (1)

		INSERT INTO PassagierVoorVlucht VALUES (1, 10, 1, NULL, NULL)
		INSERT INTO PassagierVoorVlucht VALUES (2, 10, 1, NULL, NULL)
		PRINT 'GOED - Constraint 7 - Single insert 2 passagiers zonder tijdstip en zonder stoel - inserted'
	END TRY
	BEGIN CATCH
		PRINT 'FOUT - Constraint 7 - Single insert 2 passagiers zonder tijdstip en zonder stoel - NOT inserted'
	END CATCH
ROLLBACK TRAN


BEGIN TRAN
	BEGIN TRY
		INSERT INTO Passagier VALUES (1, 'harm', 'M', '1990-12-31 00:00:00.000')
		INSERT INTO Passagier VALUES (2, 'jan', 'M', '1992-01-07 00:00:00.000')
		INSERT INTO Gate VALUES ('A')
		INSERT INTO Maatschappij VALUES ('KL', 'KLM')
		INSERT INTO Luchthaven VALUES ('DUB', 'Dublin Airport', 'Ierland')
		INSERT INTO Vliegtuig VALUES ('Boeing 747')
		INSERT INTO Vlucht VALUES (10, 'A', 'KL', 'DUB', 'Boeing 747', 120, 2500, 20.00, '2004-01-30 11:30:00.000', '2004-01-31 17:30:00.000')
		INSERT INTO Balie VALUES (1)

		INSERT INTO PassagierVoorVlucht VALUES (1, 10, 1, NULL, NULL),
												(2, 10, 1, NULL, NULL)
		PRINT 'GOED - Constraint 7 - Multi insert 2 passagiers zonder tijdstip en zonder stoel - inserted'
	END TRY
	BEGIN CATCH
		PRINT 'FOUT - Constraint 7 - Multi insert 2 passagiers zonder tijdstip en zonder stoel - NOT inserted'
	END CATCH
ROLLBACK TRAN


BEGIN TRAN
	BEGIN TRY
		INSERT INTO Passagier VALUES (1, 'harm', 'M', '1990-12-31 00:00:00.000')
		INSERT INTO Gate VALUES ('A')
		INSERT INTO Maatschappij VALUES ('KL', 'KLM')
		INSERT INTO Luchthaven VALUES ('DUB', 'Dublin Airport', 'Ierland')
		INSERT INTO Vliegtuig VALUES ('Boeing 747')
		INSERT INTO Vlucht VALUES (10, 'A', 'KL', 'DUB', 'Boeing 747', 120, 2500, 20.00, '2004-01-30 11:30:00.000', '2004-01-31 17:30:00.000')
		INSERT INTO Balie VALUES (1)

		INSERT INTO PassagierVoorVlucht VALUES (1, 10, 1, '2004-01-01 13:37:00.000', 2)
		PRINT 'GOED - Constraint 7 - Single insert met tijdstip en met stoel - inserted'
	END TRY
	BEGIN CATCH
		PRINT 'FOUT - Constraint 7 - Single insert met tijdstip en met stoel - NOT inserted'
	END CATCH
ROLLBACK TRAN


BEGIN TRAN
	BEGIN TRY
		INSERT INTO Passagier VALUES (1, 'harm', 'M', '1990-12-31 00:00:00.000')
		INSERT INTO Passagier VALUES (2, 'jan', 'M', '1992-01-07 00:00:00.000')
		INSERT INTO Gate VALUES ('A')
		INSERT INTO Maatschappij VALUES ('KL', 'KLM')
		INSERT INTO Luchthaven VALUES ('DUB', 'Dublin Airport', 'Ierland')
		INSERT INTO Vliegtuig VALUES ('Boeing 747')
		INSERT INTO Vlucht VALUES (10, 'A', 'KL', 'DUB', 'Boeing 747', 120, 2500, 20.00, '2004-01-30 11:30:00.000', '2004-01-31 17:30:00.000')
		INSERT INTO Balie VALUES (1)

		INSERT INTO PassagierVoorVlucht VALUES (1, 10, 1, '2004-01-01 13:37:00.000', 2)
		INSERT INTO PassagierVoorVlucht VALUES (2, 10, 1, '2004-01-01 13:37:00.000', 3)

		PRINT 'GOED - Constraint 7 - Single insert 2 passagiers, verschillende stoelnummers - inserted'
	END TRY
	BEGIN CATCH
		PRINT 'FOUT - Constraint 7 - Single insert 2 passagiers, verschillende stoelnummers - NOT inserted'
	END CATCH
ROLLBACK TRAN


BEGIN TRAN
	BEGIN TRY
		INSERT INTO Passagier VALUES (1, 'harm', 'M', '1990-12-31 00:00:00.000')
		INSERT INTO Passagier VALUES (2, 'jan', 'M', '1992-01-07 00:00:00.000')
		INSERT INTO Gate VALUES ('A')
		INSERT INTO Maatschappij VALUES ('KL', 'KLM')
		INSERT INTO Luchthaven VALUES ('DUB', 'Dublin Airport', 'Ierland')
		INSERT INTO Vliegtuig VALUES ('Boeing 747')
		INSERT INTO Vlucht VALUES (10, 'A', 'KL', 'DUB', 'Boeing 747', 120, 2500, 20.00, '2004-01-30 11:30:00.000', '2004-01-31 17:30:00.000')
		INSERT INTO Balie VALUES (1)

		INSERT INTO PassagierVoorVlucht VALUES (1, 10, 1, '2004-01-01 13:37:00.000', 2),
												(2, 10, 1, '2004-01-01 13:37:00.000', 3)

		PRINT 'GOED - Constraint 7 - Multi insert 2 passagiers, verschillende stoelnummers - inserted'
	END TRY
	BEGIN CATCH
		PRINT 'FOUT - Constraint 7 - Multi insert 2 passagiers, verschillende stoelnummers - NOT inserted'
	END CATCH
ROLLBACK TRAN

BEGIN TRAN
	BEGIN TRY
		INSERT INTO Passagier VALUES (1, 'harm', 'M', '1990-12-31 00:00:00.000')
		INSERT INTO Passagier VALUES (2, 'jan', 'M', '1992-01-07 00:00:00.000')
		INSERT INTO Gate VALUES ('A')
		INSERT INTO Maatschappij VALUES ('KL', 'KLM')
		INSERT INTO Luchthaven VALUES ('DUB', 'Dublin Airport', 'Ierland')
		INSERT INTO Vliegtuig VALUES ('Boeing 747')
		INSERT INTO Vlucht VALUES (10, 'A', 'KL', 'DUB', 'Boeing 747', 120, 2500, 20.00, '2004-01-30 11:30:00.000', '2004-01-31 17:30:00.000')
		INSERT INTO Balie VALUES (1)

		INSERT INTO PassagierVoorVlucht VALUES (1, 10, 1, '2004-01-01 13:37:00.000', 2)
		INSERT INTO PassagierVoorVlucht VALUES (2, 10, 1, '2004-01-01 13:37:00.000', 2)
		PRINT 'FOUT - Constraint 7 - Single insert 2 passagiers, zelfde stoelnummers - inserted'
	END TRY
	BEGIN CATCH
		PRINT 'GOED - Constraint 7 - Single insert 2 passagiers, zelfde stoelnummers - NOT inserted'
	END CATCH
ROLLBACK TRAN


BEGIN TRAN
	BEGIN TRY
		INSERT INTO Passagier VALUES (1, 'harm', 'M', '1990-12-31 00:00:00.000')
		INSERT INTO Passagier VALUES (2, 'jan', 'M', '1992-01-07 00:00:00.000')
		INSERT INTO Gate VALUES ('A')
		INSERT INTO Maatschappij VALUES ('KL', 'KLM')
		INSERT INTO Luchthaven VALUES ('DUB', 'Dublin Airport', 'Ierland')
		INSERT INTO Vliegtuig VALUES ('Boeing 747')
		INSERT INTO Vlucht VALUES (10, 'A', 'KL', 'DUB', 'Boeing 747', 120, 2500, 20.00, '2004-01-30 11:30:00.000', '2004-01-31 17:30:00.000')
		INSERT INTO Balie VALUES (1)

		INSERT INTO PassagierVoorVlucht VALUES (1, 10, 1, '2004-01-01 13:37:00.000', 2),
												(2, 10, 1, '2004-01-01 13:37:00.000', 2)
		PRINT 'FOUT - Constraint 7 - Multi insert 2 passagiers, zelfde stoelnummers - inserted'
	END TRY
	BEGIN CATCH
		PRINT 'GOED - Constraint 7 - Multi insert 2 passagiers, zelfde stoelnummers - NOT inserted'
	END CATCH
ROLLBACK TRAN