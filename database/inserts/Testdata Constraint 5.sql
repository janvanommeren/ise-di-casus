-- Het totaalgewicht van de bagage van een passagier mag het maximaal per persoon toegestane gewicht op een vlucht niet overschrijden.

USE gelre_airport
SET NOCOUNT ON


BEGIN TRAN
	BEGIN TRY
		INSERT INTO Passagier VALUES (2, 'jan', 'M', '1992-01-07 00:00:00.000')
		INSERT INTO Gate VALUES ('A')
		INSERT INTO Maatschappij VALUES ('KL', 'KLM')
		INSERT INTO Luchthaven VALUES ('DUB', 'Dublin Airport', 'Ierland')
		INSERT INTO Vliegtuig VALUES ('Boeing 747')
		INSERT INTO Vlucht VALUES (10, 'A', 'KL', 'DUB', 'Boeing 747', 120, 2500, 20.00, '2004-01-30 11:30:00.000', '2004-01-31 17:30:00.000')
		INSERT INTO Balie VALUES (1)

		INSERT INTO PassagierVoorVlucht	VALUES	(2, 10, 1, '2004-01-30 10:30:00.000', 20)
		
		INSERT INTO Object (passagiernummer, vluchtnummer, gewicht) VALUES (2, 10, 15);
		PRINT 'GOED - Constraint 5 - Single insert object met 15 gewicht - inserted'
	END TRY
	BEGIN CATCH
		PRINT 'FOUT - Constraint 5 - Single insert object met 15 gewicht - NOT inserted'
		PRINT ERROR_MESSAGE();
	END CATCH
ROLLBACK TRAN


BEGIN TRAN
	BEGIN TRY
		INSERT INTO Passagier VALUES (1, 'harm', 'M', '1990-12-31 00:00:00.000')
		INSERT INTO Gate VALUES ('A')
		INSERT INTO Maatschappij VALUES ('KL', 'KLM')
		INSERT INTO Luchthaven VALUES ('DUB', 'Dublin Airport', 'Ierland')
		INSERT INTO Vliegtuig VALUES ('Boeing 747')
		INSERT INTO Vlucht VALUES (10, 'A', 'KL', 'DUB', 'Boeing 747', 120, 2500, 20.00, '2004-01-30 11:30:00.000', '2004-01-31 17:30:00.000')
		INSERT INTO Balie VALUES (1)
		INSERT INTO PassagierVoorVlucht	VALUES	(1, 10, 1, '2004-01-30 10:30:00.000', 20)
	
		INSERT INTO Object (passagiernummer, vluchtnummer, gewicht) VALUES (1, 10, 20);
		PRINT 'GOED - Constraint 5 - Single insert object met maximum gewicht - inserted'
	END TRY
	BEGIN CATCH
		PRINT 'FOUT - Constraint 5 - Single insert object met maximum gewicht - NOT inserted'
	END CATCH
ROLLBACK TRAN


BEGIN TRAN
	BEGIN TRY
		INSERT INTO Passagier VALUES (1, 'harm', 'M', '1990-12-31 00:00:00.000')
		INSERT INTO Gate VALUES ('A')
		INSERT INTO Maatschappij VALUES ('KL', 'KLM')
		INSERT INTO Luchthaven VALUES ('DUB', 'Dublin Airport', 'Ierland')
		INSERT INTO Vliegtuig VALUES ('Boeing 747')
		INSERT INTO Vlucht VALUES (10, 'A', 'KL', 'DUB', 'Boeing 747', 120, 2500, 20.00, '2004-01-30 11:30:00.000', '2004-01-31 17:30:00.000')
		INSERT INTO Balie VALUES (1)
		INSERT INTO PassagierVoorVlucht	VALUES	(1, 10, 1, '2004-01-30 10:30:00.000', 20)
	
		INSERT INTO Object (passagiernummer, vluchtnummer, gewicht) VALUES (1, 10, 5);
		INSERT INTO Object (passagiernummer, vluchtnummer, gewicht) VALUES (1, 10, 14);
		INSERT INTO Object (passagiernummer, vluchtnummer, gewicht) VALUES (1, 10, 1);

		PRINT 'GOED - Constraint 5 - Meerdere inserts naar maximum gewicht - inserted'
	END TRY
	BEGIN CATCH
		PRINT 'FOUT - Constraint 5 - Meerdere inserts naar maximum gewicht - NOT inserted'
	END CATCH
ROLLBACK TRAN


BEGIN TRAN
	BEGIN TRY
		INSERT INTO Passagier VALUES (1, 'harm', 'M', '1990-12-31 00:00:00.000')
		INSERT INTO Gate VALUES ('A')
		INSERT INTO Maatschappij VALUES ('KL', 'KLM')
		INSERT INTO Luchthaven VALUES ('DUB', 'Dublin Airport', 'Ierland')
		INSERT INTO Vliegtuig VALUES ('Boeing 747')
		INSERT INTO Vlucht VALUES (10, 'A', 'KL', 'DUB', 'Boeing 747', 120, 2500, 20.00, '2004-01-30 11:30:00.000', '2004-01-31 17:30:00.000')
		INSERT INTO Balie VALUES (1)
		INSERT INTO PassagierVoorVlucht	VALUES	(1, 10, 1, '2004-01-30 10:30:00.000', 20)
	
		INSERT INTO Object (passagiernummer, vluchtnummer, gewicht) VALUES (1, 10, 5),
																			(1, 10, 14),
																			(1, 10, 1)

		PRINT 'GOED - Constraint 5 - Multiple inserts naar maximum gewicht - inserted'
	END TRY
	BEGIN CATCH
		PRINT 'FOUT - Constraint 5 - Multiple inserts naar maximum gewicht - NOT inserted'
	END CATCH
ROLLBACK TRAN



BEGIN TRAN
	BEGIN TRY
		INSERT INTO Passagier VALUES (1, 'harm', 'M', '1990-12-31 00:00:00.000')
		INSERT INTO Gate VALUES ('A')
		INSERT INTO Maatschappij VALUES ('KL', 'KLM')
		INSERT INTO Luchthaven VALUES ('DUB', 'Dublin Airport', 'Ierland')
		INSERT INTO Vliegtuig VALUES ('Boeing 747')
		INSERT INTO Vlucht VALUES (10, 'A', 'KL', 'DUB', 'Boeing 747', 120, 2500, 20.00, '2004-01-30 11:30:00.000', '2004-01-31 17:30:00.000')
		INSERT INTO Balie VALUES (1)
		INSERT INTO PassagierVoorVlucht	VALUES	(1, 10, 1, '2004-01-30 10:30:00.000', 20)
	
		INSERT INTO Object (passagiernummer, vluchtnummer, gewicht) VALUES (1, 10, 21);
		PRINT 'FOUT - Constraint 5 - Single insert object met 1 over maximum gewicht - inserted'
	END TRY
	BEGIN CATCH
		PRINT 'GOED - Constraint 5 - Single insert object met 1 over maximum gewicht - NOT inserted'
	END CATCH
ROLLBACK TRAN


BEGIN TRAN
	BEGIN TRY
		INSERT INTO Passagier VALUES (1, 'harm', 'M', '1990-12-31 00:00:00.000')
		INSERT INTO Gate VALUES ('A')
		INSERT INTO Maatschappij VALUES ('KL', 'KLM')
		INSERT INTO Luchthaven VALUES ('DUB', 'Dublin Airport', 'Ierland')
		INSERT INTO Vliegtuig VALUES ('Boeing 747')
		INSERT INTO Vlucht VALUES (10, 'A', 'KL', 'DUB', 'Boeing 747', 120, 2500, 20.00, '2004-01-30 11:30:00.000', '2004-01-31 17:30:00.000')
		INSERT INTO Balie VALUES (1)
		INSERT INTO PassagierVoorVlucht	VALUES	(1, 10, 1, '2004-01-30 10:30:00.000', 20)
	
		INSERT INTO Object (passagiernummer, vluchtnummer, gewicht) VALUES (1, 10, 5);
		INSERT INTO Object (passagiernummer, vluchtnummer, gewicht) VALUES (1, 10, 14);
		INSERT INTO Object (passagiernummer, vluchtnummer, gewicht) VALUES (1, 10, 2);
		PRINT 'FOUT - Constraint 5 - Meerdere inserts naar 1 over maximum gewicht - NOT inserted'
	END TRY
	BEGIN CATCH
		PRINT 'GOED - Constraint 5 - Meerdere inserts naar 1 over maximum gewicht - NOT inserted'
	END CATCH
ROLLBACK TRAN


BEGIN TRAN
	BEGIN TRY
		INSERT INTO Passagier VALUES (1, 'harm', 'M', '1990-12-31 00:00:00.000')
		INSERT INTO Gate VALUES ('A')
		INSERT INTO Maatschappij VALUES ('KL', 'KLM')
		INSERT INTO Luchthaven VALUES ('DUB', 'Dublin Airport', 'Ierland')
		INSERT INTO Vliegtuig VALUES ('Boeing 747')
		INSERT INTO Vlucht VALUES (10, 'A', 'KL', 'DUB', 'Boeing 747', 120, 2500, 20.00, '2004-01-30 11:30:00.000', '2004-01-31 17:30:00.000')
		INSERT INTO Balie VALUES (1)
		INSERT INTO PassagierVoorVlucht	VALUES	(1, 10, 1, '2004-01-30 10:30:00.000', 20)
	
		INSERT INTO Object (passagiernummer, vluchtnummer, gewicht) VALUES (1, 10, 5),
																			(1, 10, 14),
																			(1, 10, 2);
		PRINT 'FOUT - Constraint 5 - Multiple inserts naar 1 over maximum gewicht - inserted'
	END TRY
	BEGIN CATCH
		PRINT 'GOED - Constraint 5 - Multiple inserts naar 1 over maximum gewicht - NOT inserted'
	END CATCH
ROLLBACK TRAN
