-- Testdata Constraint 3
-- Het inchecktijdstip van een passagier moet v��r het vertrektijdstip van een vlucht liggen.

USE gelre_airport
SET NOCOUNT ON

BEGIN TRAN -- GOED
	BEGIN TRY
		INSERT INTO Passagier VALUES (1, 'harm', 'M', '1990-12-31 00:00:00.000')
		INSERT INTO Gate VALUES ('A')
		INSERT INTO Maatschappij VALUES ('KL', 'KLM')
		INSERT INTO Luchthaven VALUES ('DUB', 'Dublin Airport', 'Ierland')
		INSERT INTO Vliegtuig VALUES ('Boeing 747')
		INSERT INTO Vlucht VALUES (10, 'A', 'KL', 'DUB', 'Boeing 747', 120, 2500, 20.00, '2004-01-30 11:30:00.000', '2004-01-31 17:30:00.000')
		INSERT INTO Balie VALUES (1)

		INSERT INTO PassagierVoorVlucht	VALUES	(1, 10, 1, '2004-01-30 10:30:00.000', 80)
		PRINT 'GOED - Constraint 3 - Single insert 1 passagier 1 uur voor vertrektijd - inserted'
	END TRY
	BEGIN CATCH
		PRINT 'FOUT - Constraint 3 - Single insert 1 passagier 1 uur voor vertrektijd - NOT inserted'
	END CATCH
ROLLBACK TRAN

BEGIN TRAN -- GOED
	BEGIN TRY
		INSERT INTO Passagier VALUES (1, 'harm', 'M', '1990-12-31 00:00:00.000')
		INSERT INTO Passagier VALUES (2, 'jan', 'M', '1992-01-07 00:00:00.000')
		INSERT INTO Gate VALUES ('A')
		INSERT INTO Maatschappij VALUES ('KL', 'KLM')
		INSERT INTO Luchthaven VALUES ('DUB', 'Dublin Airport', 'Ierland')
		INSERT INTO Vliegtuig VALUES ('Boeing 747')
		INSERT INTO Vlucht VALUES (10, 'A', 'KL', 'DUB', 'Boeing 747', 120, 2500, 20.00, '2004-01-30 11:30:00.000', '2004-01-31 17:30:00.000')
		INSERT INTO Balie VALUES (1)

		INSERT INTO PassagierVoorVlucht VALUES  (1, 10, 1, '2004-01-30 10:30:00.000', 80), 
												(2, 10, 1, '2004-01-30 10:30:00.000', 81)
		PRINT 'GOED - Constraint 3 - Multi insert 2 passagiers 1 uur voor vertrektijd - inserted'
	END TRY
	BEGIN CATCH
		PRINT 'FOUT - Constraint 3 - Multi insert 2 passagiers 1 uur voor vertrektijd - NOT inserted'
	END CATCH
ROLLBACK TRAN


BEGIN TRAN -- FOUT
	BEGIN TRY
		INSERT INTO Passagier VALUES (2, 'jan', 'M', '1992-01-07 00:00:00.000')
		INSERT INTO Gate VALUES ('A')
		INSERT INTO Maatschappij VALUES ('KL', 'KLM')
		INSERT INTO Luchthaven VALUES ('DUB', 'Dublin Airport', 'Ierland')
		INSERT INTO Vliegtuig VALUES ('Boeing 747')
		INSERT INTO Vlucht VALUES (10, 'A', 'KL', 'DUB', 'Boeing 747', 120, 2500, 20.00, '2004-01-30 11:30:00.000', '2004-01-31 17:30:00.000')
		INSERT INTO Balie VALUES (1)

		INSERT INTO PassagierVoorVlucht VALUES (2, 10, 1, '2004-01-30 11:30:00.000', 80)
		PRINT 'FOUT - Constraint 3 - Single insert 1 passagier op de vertrektijd - inserted'
	END TRY
	BEGIN CATCH
		PRINT 'GOED - Constraint 3 - Single insert 1 passagier op de vertrektijd - NOT inserted'
	END CATCH
ROLLBACK TRAN


BEGIN TRAN -- FOUT
	BEGIN TRY
		INSERT INTO Passagier VALUES (1, 'harm', 'M', '1990-12-31 00:00:00.000')
		INSERT INTO Passagier VALUES (2, 'jan', 'M', '1992-01-07 00:00:00.000')
		INSERT INTO Gate VALUES ('A')
		INSERT INTO Maatschappij VALUES ('KL', 'KLM')
		INSERT INTO Luchthaven VALUES ('DUB', 'Dublin Airport', 'Ierland')
		INSERT INTO Vliegtuig VALUES ('Boeing 747')
		INSERT INTO Vlucht VALUES (10, 'A', 'KL', 'DUB', 'Boeing 747', 120, 2500, 20.00, '2004-01-30 11:30:00.000', '2004-01-31 17:30:00.000')
		INSERT INTO Balie VALUES (1)

		INSERT INTO PassagierVoorVlucht VALUES  (1, 10, 1, '2004-01-30 11:30:00.000', 80),
												(2, 10, 1, '2004-01-30 11:30:00.000', 81)
		PRINT 'FOUT - Constraint 3 - Multi insert 2 passagiers op de vertrektijd - inserted'
	END TRY
	BEGIN CATCH
		PRINT 'GOED - Constraint 3 - Multi insert 2 passagier op de vertrektijd - NOT inserted'
	END CATCH
ROLLBACK TRAN


BEGIN TRAN -- FOUT
	BEGIN TRY
		INSERT INTO Passagier VALUES (1, 'harm', 'M', '1990-12-31 00:00:00.000')
		INSERT INTO Gate VALUES ('A')
		INSERT INTO Maatschappij VALUES ('KL', 'KLM')
		INSERT INTO Luchthaven VALUES ('DUB', 'Dublin Airport', 'Ierland')
		INSERT INTO Vliegtuig VALUES ('Boeing 747')
		INSERT INTO Vlucht VALUES (10, 'A', 'KL', 'DUB', 'Boeing 747', 120, 2500, 20.00, '2004-01-30 11:30:00.000', '2004-01-31 17:30:00.000')
		INSERT INTO Balie VALUES (1)

		INSERT INTO PassagierVoorVlucht VALUES (1, 10, 1, '2004-01-30 12:30:00.000', 80)
		PRINT 'FOUT - Constraint 3 - Single insert 1 passagier 1 uur na vertrektijd - inserted'
	END TRY
	BEGIN CATCH
		PRINT 'GOED - Constraint 3 - Single insert 1 passagier 1 uur na vertrektijd - NOT inserted'
	END CATCH
ROLLBACK TRAN


BEGIN TRAN -- FOUT
	BEGIN TRY
		INSERT INTO Passagier VALUES (1, 'harm', 'M', '1990-12-31 00:00:00.000')
		INSERT INTO Passagier VALUES (2, 'jan', 'M', '1992-01-07 00:00:00.000')
		INSERT INTO Gate VALUES ('A')
		INSERT INTO Maatschappij VALUES ('KL', 'KLM')
		INSERT INTO Luchthaven VALUES ('DUB', 'Dublin Airport', 'Ierland')
		INSERT INTO Vliegtuig VALUES ('Boeing 747')
		INSERT INTO Vlucht VALUES (10, 'A', 'KL', 'DUB', 'Boeing 747', 120, 2500, 20.00, '2004-01-30 11:30:00.000', '2004-01-31 17:30:00.000')
		INSERT INTO Balie VALUES (1)

		INSERT INTO PassagierVoorVlucht VALUES  (1, 10, 1, '2004-01-30 12:30:00.000', 80),
												(2, 10, 1, '2004-01-30 12:30:00.000', 81)
		PRINT 'FOUT - Constraint 3 - Multi insert 2 passagiers 1 uur na vertrektijd - inserted'
	END TRY
	BEGIN CATCH
		PRINT 'GOED - Constraint 3 - Multi insert 2 passagiers 1 uur na vertrektijd - NOT inserted'
	END CATCH
ROLLBACK TRAN