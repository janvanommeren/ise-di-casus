-- Testdata Constraint 2
--	Als er een passagier aan een vlucht is toegevoegd mogen de gegevens van die vlucht niet meer gewijzigd worden. 	
USE gelre_airport
SET NOCOUNT ON

BEGIN TRAN

	BEGIN TRY
		INSERT INTO Gate VALUES ('A')
		INSERT INTO Maatschappij VALUES ('KL', 'KLM')
		INSERT INTO Luchthaven VALUES ('DUB', 'Dublin Airport', 'Ierland')
		INSERT INTO Vliegtuig VALUES ('Boeing 747')

		INSERT INTO Vlucht VALUES (10, 'A', 'KL', 'DUB', 'Boeing 747', 120, 2500, 20.00, '2004-01-30 11:30:00.000', '2004-01-31 17:30:00.000')
		PRINT 'GOED - Constraint 2 - Testvlucht aangemaakt'
	END TRY
	BEGIN CATCH
		PRINT 'FOUT - Constraint 2 - Testvlucht NIET aangemaakt'
	END CATCH


	BEGIN TRY
		INSERT INTO Gate VALUES ('B')
		UPDATE Vlucht 
		SET gatecode = 'B'
		WHERE vluchtnummer = 10
		IF @@ROWCOUNT = 0	
			PRINT 'FOUT - Constraint 2 - Update vlucht zonder passagiers - NOT updated'
		ELSE
			PRINT 'GOED - Constraint 2 - Update vlucht zonder passagiers - updated'
	END TRY
	BEGIN CATCH
		PRINT 'FOUT - Constraint 2 - Update vlucht zonder passagiers - NOT updated'
	END CATCH


	BEGIN TRY
		INSERT INTO Passagier VALUES (2, 'jan', 'M', '1992-01-07 00:00:00.000')
		INSERT INTO Balie VALUES (1)

		INSERT INTO PassagierVoorVlucht	VALUES	(2, 10, 1, '2004-01-29 22:25:00.000', 12)
		PRINT 'GOED - Constraint 2 - Insert Testpassagier in Testvlucht - inserted'
	END TRY
	BEGIN CATCH
		PRINT 'FOUT - Constraint 2 - Insert Testpassagier in Testvlucht - NOT inserted'
	END CATCH
	

	BEGIN TRY
		UPDATE Vlucht
		SET gatecode = 'C'
		WHERE vluchtnummer = 10
		IF @@ROWCOUNT = 0
			PRINT 'GOED - Constraint 2 - Update vlucht met passagiers - NOT updated'
		ELSE
			PRINT 'FOUT - Constraint 2 - Update vlucht met passagiers - updated'
	END TRY
	BEGIN CATCH
		PRINT 'GOED - Constraint 2 - Update vlucht met passagiers - NOT updated'
	END CATCH
	GO

ROLLBACK TRAN