-- Testdata Constraint 1
-- Als er een passagier aan een vlucht is toegevoegd mogen de gegevens van die vlucht niet meer gewijzigd worden. 	

USE gelre_airport
SET NOCOUNT ON

BEGIN TRAN
	BEGIN TRY
		INSERT INTO Passagier VALUES (1, 'harm', 'M', '1990-12-31 00:00:00.000')
		INSERT INTO Gate VALUES ('A')
		INSERT INTO Maatschappij VALUES ('KL', 'KLM')
		INSERT INTO Luchthaven VALUES ('DUB', 'Dublin Airport', 'Ierland')
		INSERT INTO Vliegtuig VALUES ('Boeing 747')
		INSERT INTO Vlucht VALUES (10, 'A', 'KL', 'DUB', 'Boeing 747', 120, 2500, 20.00, '2004-01-30 11:30:00.000', '2004-01-31 17:30:00.000')
		INSERT INTO Balie VALUES (1)
		
		INSERT INTO PassagierVoorVlucht	VALUES	(1, 10, 1, '2004-01-29 22:25:00.000', 75)
		PRINT 'GOED - Constraint 1 - Single insert - inserted' 
	END TRY
	BEGIN CATCH
		PRINT 'FOUT - Constraint 1 - Single insert - NOT inserted'
	END CATCH
ROLLBACK TRAN 


BEGIN TRAN
	BEGIN TRY
		INSERT INTO Passagier VALUES (1, 'harm', 'M', '1990-12-31 00:00:00.000')
		INSERT INTO Passagier VALUES (2, 'jan', 'M', '1992-01-07 00:00:00.000')
		INSERT INTO Gate VALUES ('A')
		INSERT INTO Maatschappij VALUES ('KL', 'KLM')
		INSERT INTO Luchthaven VALUES ('DUB', 'Dublin Airport', 'Ierland')
		INSERT INTO Vliegtuig VALUES ('Boeing 747')
		INSERT INTO Vlucht VALUES (10, 'A', 'KL', 'DUB', 'Boeing 747', 120, 2500, 20.00, '2004-01-30 11:30:00.000', '2004-01-31 17:30:00.000')
		INSERT INTO Balie VALUES (1)

		INSERT INTO PassagierVoorVlucht	VALUES	
			(1, 10, 1, '2004-01-29 22:25:00.000', 75),
			(2, 10, 1, '2004-01-29 22:25:00.000', 76)
		PRINT 'GOED - Constraint 1 - Multi insert volledige data - inserted'
	END TRY
	BEGIN CATCH
		PRINT 'FOUT - Constraint 1 - Multi insert volledige data - NOT inserted'
	END CATCH
ROLLBACK TRAN


BEGIN TRAN
	BEGIN TRY
		INSERT INTO Passagier VALUES (1, 'harm', 'M', '1990-12-31 00:00:00.000')
		INSERT INTO Gate VALUES ('A')
		INSERT INTO Maatschappij VALUES ('KL', 'KLM')
		INSERT INTO Luchthaven VALUES ('DUB', 'Dublin Airport', 'Ierland')
		INSERT INTO Vliegtuig VALUES ('Boeing 747')
		INSERT INTO Vlucht VALUES (10, 'A', 'KL', 'DUB', 'Boeing 747', 120, 2500, 20.00, '2004-01-30 11:30:00.000', '2004-01-31 17:30:00.000')
		INSERT INTO Balie VALUES (1)

		INSERT INTO PassagierVoorVlucht	VALUES	(1, 10, 1, NULL, NULL)
		PRINT 'GOED - Constraint 1 - Single insert zonder datum/tijd en zonder stoel - inserted'
	END TRY
	BEGIN CATCH
		PRINT 'FOUT - Constraint 1 - Single insert zonder datum/tijd en zonder stoel - NOT inserted'
	END CATCH
ROLLBACK TRAN


BEGIN TRAN
	BEGIN TRY
		INSERT INTO Passagier VALUES (1, 'harm', 'M', '1990-12-31 00:00:00.000')
		INSERT INTO Passagier VALUES (2, 'jan', 'M', '1992-01-07 00:00:00.000')
		INSERT INTO Gate VALUES ('A')
		INSERT INTO Maatschappij VALUES ('KL', 'KLM')
		INSERT INTO Luchthaven VALUES ('DUB', 'Dublin Airport', 'Ierland')
		INSERT INTO Vliegtuig VALUES ('Boeing 747')
		INSERT INTO Vlucht VALUES (10, 'A', 'KL', 'DUB', 'Boeing 747', 120, 2500, 20.00, '2004-01-30 11:30:00.000', '2004-01-31 17:30:00.000')
		INSERT INTO Balie VALUES (1)

		INSERT INTO PassagierVoorVlucht	VALUES	
			(1, 10, 1, NULL, NULL),
			(2, 10, 1, NULL, NULL)
		PRINT 'GOED - Constraint 1 - Multi insert zonder datum/tijd en zonder stoel - inserted' 
	END TRY
	BEGIN CATCH
		PRINT 'FOUT - Constraint 1 - Multi insert zonder datum/tijd en zonder stoel - NOT inserted'
	END CATCH
ROLLBACK TRAN


BEGIN TRAN
	BEGIN TRY
		INSERT INTO Passagier VALUES (1, 'harm', 'M', '1990-12-31 00:00:00.000')
		INSERT INTO Gate VALUES ('A')
		INSERT INTO Maatschappij VALUES ('KL', 'KLM')
		INSERT INTO Luchthaven VALUES ('DUB', 'Dublin Airport', 'Ierland')
		INSERT INTO Vliegtuig VALUES ('Boeing 747')
		INSERT INTO Vlucht VALUES (10, 'A', 'KL', 'DUB', 'Boeing 747', 120, 2500, 20.00, '2004-01-30 11:30:00.000', '2004-01-31 17:30:00.000')
		INSERT INTO Balie VALUES (1)

		INSERT INTO PassagierVoorVlucht	VALUES	(1, 10, 1, NULL, 75)
		PRINT 'FOUT - Constraint 1 - Single insert zonder datum/tijd en met stoel - inserted'
	END TRY
	BEGIN CATCH
		PRINT 'GOED - Constraint 1 - Single insert zonder datum/tijd en met stoel - NOT inserted'
	END CATCH
ROLLBACK TRAN


BEGIN TRAN
	BEGIN TRY
		INSERT INTO Passagier VALUES (1, 'harm', 'M', '1990-12-31 00:00:00.000')
		INSERT INTO Passagier VALUES (2, 'jan', 'M', '1992-01-07 00:00:00.000')
		INSERT INTO Gate VALUES ('A')
		INSERT INTO Maatschappij VALUES ('KL', 'KLM')
		INSERT INTO Luchthaven VALUES ('DUB', 'Dublin Airport', 'Ierland')
		INSERT INTO Vliegtuig VALUES ('Boeing 747')
		INSERT INTO Vlucht VALUES (10, 'A', 'KL', 'DUB', 'Boeing 747', 120, 2500, 20.00, '2004-01-30 11:30:00.000', '2004-01-31 17:30:00.000')
		INSERT INTO Balie VALUES (1)

		INSERT INTO PassagierVoorVlucht	VALUES	
			(1, 10, 1, NULL, 75),
			(2, 10, 1, NULL, 76)
		PRINT 'FOUT - Constraint 1 - Multi insert zonder datum/tijd en met stoel - inserted'
	END TRY
	BEGIN CATCH
		PRINT 'GOED - Constraint 1 - Multi insert zonder datum/tijd en met stoel - NOT inserted'
	END CATCH
ROLLBACK TRAN


BEGIN TRAN
	BEGIN TRY
		INSERT INTO Passagier VALUES (1, 'harm', 'M', '1990-12-31 00:00:00.000')
		INSERT INTO Gate VALUES ('A')
		INSERT INTO Maatschappij VALUES ('KL', 'KLM')
		INSERT INTO Luchthaven VALUES ('DUB', 'Dublin Airport', 'Ierland')
		INSERT INTO Vliegtuig VALUES ('Boeing 747')
		INSERT INTO Vlucht VALUES (10, 'A', 'KL', 'DUB', 'Boeing 747', 120, 2500, 20.00, '2004-01-30 11:30:00.000', '2004-01-31 17:30:00.000')
		INSERT INTO Balie VALUES (1)

		INSERT INTO PassagierVoorVlucht	VALUES	(1, 10, 1, '2004-01-29 22:25:00.000', NULL)
		PRINT 'FOUT - Constraint 1 - Single insert met datum/tijd en zonder stoel - inserted'
	END TRY
	BEGIN CATCH
		PRINT 'GOED - Constraint 1 - Single insert met datum/tijd en zonder stoel - NOT inserted'
	END CATCH
ROLLBACK TRAN


BEGIN TRAN
	BEGIN TRY
		INSERT INTO Passagier VALUES (1, 'harm', 'M', '1990-12-31 00:00:00.000')
		INSERT INTO Passagier VALUES (2, 'jan', 'M', '1992-01-07 00:00:00.000')
		INSERT INTO Gate VALUES ('A')
		INSERT INTO Maatschappij VALUES ('KL', 'KLM')
		INSERT INTO Luchthaven VALUES ('DUB', 'Dublin Airport', 'Ierland')
		INSERT INTO Vliegtuig VALUES ('Boeing 747')
		INSERT INTO Vlucht VALUES (10, 'A', 'KL', 'DUB', 'Boeing 747', 120, 2500, 20.00, '2004-01-30 11:30:00.000', '2004-01-31 17:30:00.000')
		INSERT INTO Balie VALUES (1)

		INSERT INTO PassagierVoorVlucht	VALUES	
			(1, 10, 1, '2004-01-29 22:25:00.000', NULL),
			(2, 10, 1, '2004-01-29 22:25:00.000', NULL)
		PRINT 'FOUT - Constraint 1 - Multi insert met datum/tijd en zonder stoel - inserted'
	END TRY
	BEGIN CATCH
		PRINT 'GOED - Constraint 1 - Multi insert met datum/tijd en zonder stoel - NOT inserted'
	END CATCH
ROLLBACK TRAN