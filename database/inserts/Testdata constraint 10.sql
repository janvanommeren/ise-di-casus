-- Testdata Constraint 10 (CHECK)
-- Een passagier mag niet boeken op  vluchten in overlappende periodes.
USE gelre_airport
SET NOCOUNT ON

BEGIN TRAN
	BEGIN TRY
		INSERT INTO Passagier VALUES (2, 'Jan', 'M', '1993-09-15 00:00:00.000')
		INSERT INTO Gate VALUES ('A')
		INSERT INTO Maatschappij VALUES ('KL', 'KLM')
		INSERT INTO Luchthaven VALUES ('DUB', 'Dublin Airport', 'Ierland')
		INSERT INTO Vliegtuig VALUES ('Boeing 747')
		INSERT INTO Vlucht VALUES (10, 'A', 'KL', 'DUB', 'Boeing 747', 120, 2500, 20.00, '2004-01-30 11:30:00.000', '2004-01-31 17:30:00.000')
		INSERT INTO Balie VALUES (1)
		
		INSERT INTO PassagierVoorVlucht	VALUES	(2, 10, 1, '2004-01-29 22:25:00.000', 75)
		PRINT 'GOED - Constraint 1 - Single insert boeken voor enkele vlucht - inserted' 
	END TRY
	BEGIN CATCH
		PRINT 'FOUT - Constraint 1 - Single insert boeken voor enkele vlucht- NOT inserted'
	END CATCH
ROLLBACK TRAN


BEGIN TRAN
	BEGIN TRY
		INSERT INTO Passagier VALUES (2, 'Jan', 'M', '1993-09-15 00:00:00.000')
		INSERT INTO Gate VALUES ('A')
		INSERT INTO Maatschappij VALUES ('KL', 'KLM')
		INSERT INTO Luchthaven VALUES ('DUB', 'Dublin Airport', 'Ierland')
		INSERT INTO Vliegtuig VALUES ('Boeing 747')
		INSERT INTO Vlucht VALUES (10, 'A', 'KL', 'DUB', 'Boeing 747', 120, 2500, 20.00, '2004-01-30 11:30:00.000', '2004-01-31 17:30:00.000')
		INSERT INTO Vlucht VALUES (11, 'A', 'KL', 'DUB', 'Boeing 747', 120, 2500, 20.00, '2004-01-20 11:30:00.000', '2004-01-29 17:30:00.000')
		INSERT INTO Balie VALUES (1)
		
		INSERT INTO PassagierVoorVlucht	VALUES	(2, 10, 1, '2004-01-29 22:25:00.000', 75)
		INSERT INTO PassagierVoorVlucht	VALUES	(2, 11, 1, '2004-01-19 22:25:00.000', 75)
		PRINT 'GOED - Constraint 1 - Insert boeken voor niet overlappende vlucht - inserted' 
	END TRY
	BEGIN CATCH
		PRINT 'FOUT - Constraint 1 - Insert boeken voor niet overlappende vlucht - NOT inserted'
	END CATCH
ROLLBACK TRAN 


BEGIN TRAN
	BEGIN TRY
		INSERT INTO Passagier VALUES (2, 'Jan', 'M', '1993-09-15 00:00:00.000')
		INSERT INTO Gate VALUES ('A')
		INSERT INTO Maatschappij VALUES ('KL', 'KLM')
		INSERT INTO Luchthaven VALUES ('DUB', 'Dublin Airport', 'Ierland')
		INSERT INTO Vliegtuig VALUES ('Boeing 747')
		INSERT INTO Vlucht VALUES (10, 'A', 'KL', 'DUB', 'Boeing 747', 120, 2500, 20.00, '2004-01-20 11:30:00.000', '2004-01-29 17:30:00.000')
		INSERT INTO Vlucht VALUES (11, 'A', 'KL', 'DUB', 'Boeing 747', 120, 2500, 20.00, '2004-01-30 11:30:00.000', '2004-01-31 17:30:00.000')
		INSERT INTO Balie VALUES (1)
		
		INSERT INTO PassagierVoorVlucht	VALUES	(2, 10, 1, '2004-01-19 22:25:00.000', 75)
		INSERT INTO PassagierVoorVlucht	VALUES	(2, 11, 1, '2004-01-29 22:25:00.000', 75)
		PRINT 'GOED - Constraint 1 - Insert boeken voor niet overlappende vlucht2 - inserted' 
	END TRY
	BEGIN CATCH
		PRINT 'FOUT - Constraint 1 - Insert boeken voor niet overlappende vlucht2 - NOT inserted'
	END CATCH
ROLLBACK TRAN 


BEGIN TRAN
	BEGIN TRY
		INSERT INTO Passagier VALUES (2, 'Jan', 'M', '1993-09-15 00:00:00.000')
		INSERT INTO Gate VALUES ('A')
		INSERT INTO Maatschappij VALUES ('KL', 'KLM')
		INSERT INTO Luchthaven VALUES ('DUB', 'Dublin Airport', 'Ierland')
		INSERT INTO Vliegtuig VALUES ('Boeing 747')
		INSERT INTO Vlucht VALUES (10, 'A', 'KL', 'DUB', 'Boeing 747', 120, 2500, 20.00, '2004-01-20 11:30:00.000', '2004-01-21 17:30:00.000')
		INSERT INTO Vlucht VALUES (11, 'A', 'KL', 'DUB', 'Boeing 747', 120, 2500, 20.00, '2004-01-21 16:30:00.000', '2004-01-22 17:30:00.000')
		INSERT INTO Balie VALUES (1)
		
		INSERT INTO PassagierVoorVlucht	VALUES	(2, 10, 1, '2004-01-19 22:25:00.000', 75)
		INSERT INTO PassagierVoorVlucht	VALUES	(2, 11, 1, '2004-01-20 22:25:00.000', 75)
		PRINT 'FOUT - Constraint 1 - Insert boeken voor vlucht vertrektijd net over aankomsttijd - inserted' 
	END TRY
	BEGIN CATCH
		PRINT 'GOED - Constraint 1 - Insert boeken voor vlucht vertrektijd net over aankomsttijd - NOT inserted'
	END CATCH
ROLLBACK TRAN 


BEGIN TRAN
	BEGIN TRY
		INSERT INTO Passagier VALUES (2, 'Jan', 'M', '1993-09-15 00:00:00.000')
		INSERT INTO Gate VALUES ('A')
		INSERT INTO Maatschappij VALUES ('KL', 'KLM')
		INSERT INTO Luchthaven VALUES ('DUB', 'Dublin Airport', 'Ierland')
		INSERT INTO Vliegtuig VALUES ('Boeing 747')
		INSERT INTO Vlucht VALUES (10, 'A', 'KL', 'DUB', 'Boeing 747', 120, 2500, 20.00, '2004-01-20 11:30:00.000', '2004-01-21 17:30:00.000')
		INSERT INTO Vlucht VALUES (11, 'A', 'KL', 'DUB', 'Boeing 747', 120, 2500, 20.00, '2004-01-21 18:30:00.000', '2004-01-22 17:30:00.000')
		INSERT INTO Balie VALUES (1)
		
		INSERT INTO PassagierVoorVlucht	VALUES	(2, 10, 1, '2004-01-19 22:25:00.000', 75)
		INSERT INTO PassagierVoorVlucht	VALUES	(2, 11, 1, '2004-01-20 22:25:00.000', 75)
		PRINT 'GOED - Constraint 1 - Insert boeken voor vlucht vertrektijd net voor aankomsttijd - inserted' 
	END TRY
	BEGIN CATCH
		PRINT 'FOUT - Constraint 1 - Insert boeken voor vlucht vertrektijd net voor aankomsttijd - NOT inserted'
	END CATCH
ROLLBACK TRAN


BEGIN TRAN
	BEGIN TRY
		INSERT INTO Passagier VALUES (2, 'Jan', 'M', '1993-09-15 00:00:00.000')
		INSERT INTO Gate VALUES ('A')
		INSERT INTO Maatschappij VALUES ('KL', 'KLM')
		INSERT INTO Luchthaven VALUES ('DUB', 'Dublin Airport', 'Ierland')
		INSERT INTO Vliegtuig VALUES ('Boeing 747')
		INSERT INTO Vlucht VALUES (10, 'A', 'KL', 'DUB', 'Boeing 747', 120, 2500, 20.00, '2004-01-30 11:30:00.000', '2004-01-31 17:30:00.000')
		INSERT INTO Vlucht VALUES (11, 'A', 'KL', 'DUB', 'Boeing 747', 120, 2500, 20.00, '2004-01-31 11:30:00.000', '2004-02-01 17:30:00.000')
		INSERT INTO Balie VALUES (1)
		
		INSERT INTO PassagierVoorVlucht	VALUES	(2, 10, 1, '2004-01-29 22:25:00.000', 75)
		INSERT INTO PassagierVoorVlucht	VALUES	(2, 11, 1, '2004-01-29 22:25:00.000', 75)
		PRINT 'FOUT - Constraint 1 - Single insert boeken voor overlappende vlucht - inserted' 
	END TRY
	BEGIN CATCH
		PRINT 'GOED - Constraint 1 - Single insert boeken voor overlappende vlucht- NOT inserted'
	END CATCH
ROLLBACK TRAN 
