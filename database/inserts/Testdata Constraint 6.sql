-- Testdata Constraint 6
-- Elke vlucht heeft volgens de specs een toegestaan maximum aantal passagiers 
-- (map, een toegestaan maximum totaalgewicht (mt), en een maximum gewicht dat een persoon mee mag nemen (mgp). 
-- Zorg ervoor dat altijd geld map*mgp <= mt.

USE gelre_airport
SET NOCOUNT ON

BEGIN TRAN
	BEGIN TRY
		INSERT INTO Gate VALUES ('A')
		INSERT INTO Maatschappij VALUES ('KL', 'KLM')
		INSERT INTO Luchthaven VALUES ('DUB', 'Dublin Airport', 'Ierland')
		INSERT INTO Vliegtuig VALUES ('Boeing 747')
		INSERT INTO Vlucht VALUES (10, 'A', 'KL', 'DUB', 'Boeing 747', 120, 2500, 20.00, '2004-01-30 11:30:00.000', '2004-01-31 17:30:00.000')
		INSERT INTO Balie VALUES (1)

		UPDATE Vlucht SET max_totaalgewicht = 2400 WHERE vluchtnummer = 10
		IF @@ROWCOUNT = 0
			PRINT 'FOUT - Constraint 6 - Update totaalgewicht naar minimum - NOT updated'
		ELSE
			PRINT 'GOED - Constraint 6 - Update totaalgewicht naar minimum - updated'
	END TRY
	BEGIN CATCH
		PRINT 'FOUT - Constraint 6 - Update totaalgewicht naar minimum - NOT updated'
	END CATCH
ROLLBACK TRAN


BEGIN TRAN
	BEGIN TRY
		INSERT INTO Gate VALUES ('A')
		INSERT INTO Maatschappij VALUES ('KL', 'KLM')
		INSERT INTO Luchthaven VALUES ('DUB', 'Dublin Airport', 'Ierland')
		INSERT INTO Vliegtuig VALUES ('Boeing 747')
		INSERT INTO Vlucht VALUES (10, 'A', 'KL', 'DUB', 'Boeing 747', 120, 2500, 20.00, '2004-01-30 11:30:00.000', '2004-01-31 17:30:00.000')
		INSERT INTO Balie VALUES (1)

		UPDATE Vlucht SET max_aantal_psgrs = 125 WHERE vluchtnummer = 10
		IF @@ROWCOUNT = 0
			PRINT 'FOUT - Constraint 6 - Update max. aantal passagiers naar minimum - NOT updated'
		ELSE
			PRINT 'GOED - Constraint 6 - Update max. aantal passagiers naar minimum - updated'
	END TRY
	BEGIN CATCH
		PRINT 'FOUT - Constraint 6 - Update max. aantal passagiers naar minimum - NOT updated'
	END CATCH
ROLLBACK TRAN


BEGIN TRAN
	BEGIN TRY
		INSERT INTO Gate VALUES ('A')
		INSERT INTO Maatschappij VALUES ('KL', 'KLM')
		INSERT INTO Luchthaven VALUES ('DUB', 'Dublin Airport', 'Ierland')
		INSERT INTO Vliegtuig VALUES ('Boeing 747')
		INSERT INTO Vlucht VALUES (10, 'A', 'KL', 'DUB', 'Boeing 747', 120, 2500, 20.00, '2004-01-30 11:30:00.000', '2004-01-31 17:30:00.000')
		INSERT INTO Balie VALUES (1)

		UPDATE Vlucht SET max_ppgewicht = 21 WHERE vluchtnummer = 10
		IF @@ROWCOUNT = 0
			PRINT 'GOED - Constraint 6 - Update max. gewicht per persoon naar 1 over max - NOT inserted'
		ELSE
			PRINT 'FOUT - Constraint 6 - Update max. gewicht per persoon naar 1 over max - inserted'
	END TRY
	BEGIN CATCH
		PRINT 'GOED - Constraint 6 - Update max. gewicht per persoon naar 1 over max - NOT inserted'
	END CATCH
ROLLBACK TRAN


BEGIN TRAN
	BEGIN TRY
		INSERT INTO Gate VALUES ('A')
		INSERT INTO Maatschappij VALUES ('KL', 'KLM')
		INSERT INTO Luchthaven VALUES ('DUB', 'Dublin Airport', 'Ierland')
		INSERT INTO Vliegtuig VALUES ('Boeing 747')
		INSERT INTO Vlucht VALUES (10, 'A', 'KL', 'DUB', 'Boeing 747', 120, 2500, 20.00, '2004-01-30 11:30:00.000', '2004-01-31 17:30:00.000')
		INSERT INTO Balie VALUES (1)

		UPDATE Vlucht SET max_totaalgewicht = 2399 WHERE vluchtnummer = 10
		IF @@ROWCOUNT = 0
			PRINT 'GOED - Constraint 6 - Update totaal gewicht naar 1 onder minimum - NOT inserted'
		ELSE
			PRINT 'FOUT - Constraint 6 - Update totaal gewicht naar 1 onder minimum - inserted'
	END TRY
	BEGIN CATCH
		PRINT 'GOED - Constraint 6 - Update totaal gewicht naar 1 onder minimum - NOT inserted'
	END CATCH
ROLLBACK TRAN


BEGIN TRAN
	BEGIN TRY
		INSERT INTO Gate VALUES ('A')
		INSERT INTO Maatschappij VALUES ('KL', 'KLM')
		INSERT INTO Luchthaven VALUES ('DUB', 'Dublin Airport', 'Ierland')
		INSERT INTO Vliegtuig VALUES ('Boeing 747')
		INSERT INTO Vlucht VALUES (10, 'A', 'KL', 'DUB', 'Boeing 747', 120, 2500, 20.00, '2004-01-30 11:30:00.000', '2004-01-31 17:30:00.000')
		INSERT INTO Balie VALUES (1)

		UPDATE Vlucht SET max_aantal_psgrs = 126 WHERE vluchtnummer = 10
		IF @@ROWCOUNT = 0
			PRINT 'GOED - Constraint 6 - Update max aantal passagiers 1 onder max - NOT inserted'
		ELSE
			PRINT 'FOUT - Constraint 6 - Update max aantal passagiers 1 onder max - inserted'
	END TRY
	BEGIN CATCH
		PRINT 'GOED - Constraint 6 - Update max aantal passagiers 1 onder max - NOT inserted'
	END CATCH
ROLLBACK TRAN