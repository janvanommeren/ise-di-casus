
constraint 2 : vluchtnummer
    PassagierVoorVlucht
    pk passagiernummer
    pk vluchtnummer



constraint 8 : maatschappijcode
    IncheckenBijMaatschappij
    PK balienummer
    PK maatschappijcode



Mogelijk constraint 4 ook nog, daar wordt de max_totaal opgehaald.





Constraint 2
De primary key staat op passagiernummer en vluchtnummer. Omdat passagiernummer voorop staat, is het theoretisch sneller om een index aan te maken op vluchtnummer.

CREATE INDEX IX_PassagierVoorVlucht_Vluchtnummer
    ON passagierVoorVlucht (vluchtnummer)


DBCC FREEPROCCACHE
DBCC DROPCLEANBUFFERS


SELECT vluchtnummer FROM PassagierVoorVlucht

