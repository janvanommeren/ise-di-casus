-- UseCase deel 4
select p.passagiernummer, p.naam, pvv.vluchtnummer, l.naam as luchthaven, m.naam as maatschappij, CAST(v.vertrektijdstip as Date) as vertrekdatum
FROM Passagier p
	INNER JOIN PassagierVoorVlucht pvv ON pvv.passagiernummer = p.passagiernummer
	INNER JOIN Vlucht v ON v.vluchtnummer = pvv.vluchtnummer
	INNER JOIN Luchthaven l ON l.luchthavencode = v.luchthavencode
	INNER JOIN Maatschappij m ON m.maatschappijcode = v.maatschappijcode
WHERE p.passagiernummer LIKE '%%'
AND p.naam LIKE '%%'
AND pvv.vluchtnummer LIKE '%%'
AND l.naam LIKE '%%'
AND m.naam LIKE '%%'
ORDER BY p.passagiernummer

