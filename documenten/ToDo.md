#ToDo

## Taken
A. ~~In create script primary en foreign keys maken~~

B. ~~Geef van alle constraints aan welke declaratief kunnen worden gedefinieerd in CREATE TABLE of ALTER TABLE statements
Maak hiervoor een apart bestand in de map Documenten, let op het is in tekst, niet in code!~~

C. ~~Implementeer constraint 1 t/m 10~~

D. ???

## Notitie:
### Constraint 9
Wat moet er met balie gebeuren op het moment dat er een balie verwijderd wordt. IncheckenBijMaatschappij wordt automatisch verwijderd, maar dan is er een maatschappij zonder balie. Zelf hier een constraint voor schrijven of valt dit buiten onze scope?

## Planning

opdracht b: 
- bijzetten: declaratief of niet declaratief 
- checken meerdere tabellen, zo ja -> wat met deleted data bijv punt 9
- tabel namen duidelijk vermelden tussen de tekst
- dit moet een trigger worden, van maken: dit kan je met een trigger oplossen

### Harm
~~1. Opdracht A           Donderdag 26-2-2015~~
~~2. Helft opdracht C~~
  3. Opdracht E
  4. Test data constraint ...

### Jan
~~1. Opdracht B           Donderdag 26-2-2015~~
~~2. Helft opdracht C~~
  3. Opdracht D
  4. Test data constraint ...
