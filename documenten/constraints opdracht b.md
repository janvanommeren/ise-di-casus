# Opdracht B

Geef van alle constraints aan welke declaratief kunnen worden gedefinieerd in CREATE TABLE of ALTER TABLE statements. 
Met declaratief worden hier echter geen check-constraints met een user defined function bedoeld.
Geef dit aan in tekst, dus maak nog geen code.  
Motiveer je antwoord.

motivatie wat uitgebreider -> constraint 2 en 3 -> doet foreign key, is daarmee niet oplosbaar

| #  | Declaratief | Motivatie  |
|:--:|:-----------:| -----------|
| 1  | Ja          | Je kan dit door middel van een check constraint oplossen. |
| 2  | Nee         | Er moet met waardes uit verschillende tabellen gewerkt worden. |
| 3  | Nee         | Er moet met waardes uit verschillende tabellen gewerkt worden. |
| 4  | Nee         | Hier komt een select statement aan te pas voor het tellen van de passagiers. |
| 5  | Nee         | Er moet data worden opgehaald uit verschillende tabellen. |
| 6  | Ja          | De berekening kan met een check constraint worden uitgevoerd. |
| 7  | Nee         | Als stoel een null waarde bevat kunnen er dan niet meer records worden aangemaakt. -> benadrukken alternate key violation |
| 8  | Nee         | Er moet data uit verschillende tabellen worden gehaald. |
| 9  | Nee         | --- |
| 10 | Nee         | Hiervoor moet informatie op worden gehaald uit de vluchten tabel en de passagiers tabel. |