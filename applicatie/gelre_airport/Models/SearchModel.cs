﻿using System;
using System.Collections.Generic;

namespace gelre_airport.Models
{
    public partial class SearchModel
    {
        public SearchModel()
        {

        }

        public int passagiernummer { get; set; }
        public string passagiernaam { get; set; }
        public string vluchtnummer { get; set; }
        public string luchthaven { get; set; }
        public string maatschappij { get; set; }
        public DateTime vertrekdatum { get; set; }
    }
}