using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using gelre_airport.Models;
using System.Data.Entity.Infrastructure;
using System.Data.SqlClient;

namespace gelre_airport.Controllers
{
    public class PassagiersController : Controller
    {
        private GelreAirportEntities db = new GelreAirportEntities();

        // GET: Passagiers
        public ActionResult Index()
        {
            return View(db.Passagier.ToList());
        }

        // GET: Passagiers/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Passagier passagier = db.Passagier.Find(id);
            if (passagier == null)
            {
                return HttpNotFound();
            }
            return View(passagier);
        }

        public ActionResult AllDetails(int id, string vluchtnummer)
        {
            try
            {
                Passagier passagier = db.Passagier.Find(id);
                if (passagier == null)
                {
                    return HttpNotFound();
                }
                int vluchtnr = int.Parse(vluchtnummer);
                Vlucht vlucht = db.Vlucht.Find(vluchtnr);
                ViewBag.vlucht = vlucht;
                List<gelre_airport.Models.Object> baggage = new List<gelre_airport.Models.Object>();
                baggage = db.Object.Where(q => q.passagiernummer == id && q.vluchtnummer == vluchtnr).ToList();
                ViewBag.baggage = baggage;
                PassagierVoorVlucht pvv = db.PassagierVoorVlucht.Find(passagier.passagiernummer, vluchtnr);
                ViewBag.pvv = pvv;
                return View(passagier);
            } catch (InvalidOperationException e) {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
        }

        // GET: Passagiers/Create
        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Search([Bind(Include = "passagiernaam,vluchtnummer,luchthaven,maatschappij,vertrekdatum")] SearchModel searchCriteria)
        {
            if (searchCriteria.passagiernaam == null)
            {
                searchCriteria.passagiernaam = "";
            }
            if (searchCriteria.vluchtnummer == null)
            {
                searchCriteria.vluchtnummer = "";
            }
            if (searchCriteria.luchthaven == null)
            {
                searchCriteria.luchthaven = "";
            }
            if (searchCriteria.maatschappij == null)
            {
                searchCriteria.maatschappij = "";
            }
            string query = @"select p.passagiernummer as passagiernummer, p.naam as passagiernaam, CAST(pvv.vluchtnummer AS VARCHAR) as vluchtnummer, l.naam as luchthaven, m.naam as maatschappij, CAST(v.vertrektijdstip as Date) as vertrekdatum
                    FROM Passagier p
	                    INNER JOIN PassagierVoorVlucht pvv ON pvv.passagiernummer = p.passagiernummer
	                    INNER JOIN Vlucht v ON v.vluchtnummer = pvv.vluchtnummer
	                    INNER JOIN Luchthaven l ON l.luchthavencode = v.luchthavencode
	                    INNER JOIN Maatschappij m ON m.maatschappijcode = v.maatschappijcode
                    WHERE p.naam LIKE '%' + @p0 + '%'
                    AND CONVERT(VARCHAR, pvv.vluchtnummer) LIKE '%' + @p1 + '%'
                    AND l.naam LIKE '%' + @p2 + '%'
                    AND m.naam LIKE '%' + @p3 + '%' ";
            DbRawSqlQuery<SearchModel> data = null;
            if (!searchCriteria.vertrekdatum.ToString().Equals("1-1-0001 00:00:00"))
            {
                query += "AND CAST(v.vertrektijdstip AS DATE) = @p4 ORDER BY p.passagiernummer";
                data = db.Database.SqlQuery<SearchModel>(query, searchCriteria.passagiernaam, searchCriteria.vluchtnummer, 
                    searchCriteria.luchthaven, searchCriteria.maatschappij, searchCriteria.vertrekdatum.Date);
            }
            else
            {
                query += "ORDER BY p.passagiernummer";
                data = db.Database.SqlQuery<SearchModel>(query, searchCriteria.passagiernaam, 
                    searchCriteria.vluchtnummer, searchCriteria.luchthaven, searchCriteria.maatschappij);
            }
            return View(data.ToList());
        }

        // POST: Passagiers/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "passagiernummer,naam,geslacht,geboortedatum")] Passagier passagier)
        {
            if (ModelState.IsValid)
            {
                db.Passagier.Add(passagier);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(passagier);
        }

        // GET: Passagiers/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Passagier passagier = db.Passagier.Find(id);
            if (passagier == null)
            {
                return HttpNotFound();
            }
            return View(passagier);
        }

        // POST: Passagiers/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "passagiernummer,naam,geslacht,geboortedatum")] Passagier passagier)
        {
            if (ModelState.IsValid)
            {
                db.Entry(passagier).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(passagier);
        }

        // GET: Passagiers/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Passagier passagier = db.Passagier.Find(id);
            if (passagier == null)
            {
                return HttpNotFound();
            }
            return View(passagier);
        }

        // POST: Passagiers/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Passagier passagier = db.Passagier.Find(id);
            db.Passagier.Remove(passagier);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}