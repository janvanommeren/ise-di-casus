﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using gelre_airport.Models;
using System.Data.Entity.Infrastructure;

namespace gelre_airport.Controllers
{
    public class ObjectsController : Controller
    {
        private GelreAirportEntities db = new GelreAirportEntities();

        // GET: Objects
        public ActionResult Index()
        {
            return View(db.Object.ToList());
        }

        // GET: Objects/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            gelre_airport.Models.Object @object = db.Object.Find(id);
            if (@object == null)
            {
                return HttpNotFound();
            }
            return View(@object);
        }

        // GET: Objects/Create
        public ActionResult Create()
        {
            ViewBag.passagiernummer = new SelectList(db.PassagierVoorVlucht, "passagiernummer", "stoel");
            return View();
        }

        // POST: Objects/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "volgnummer,passagiernummer,vluchtnummer,gewicht")] gelre_airport.Models.Object @object)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    db.Object.Add(@object);
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }

                ViewBag.passagiernummer = new SelectList(db.PassagierVoorVlucht, "passagiernummer", "stoel", @object.passagiernummer);
                ViewBag.vluchtnummer = new SelectList(db.PassagierVoorVlucht, "vluchtnummer", "stoel", @object.vluchtnummer);
            }
            catch (DbUpdateException e)
            {
                ViewBag.error = e.GetBaseException().Message;
            }
            return View(@object);
        }

        // GET: Objects/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            gelre_airport.Models.Object @object = db.Object.Find(id);
            if (@object == null)
            {
                return HttpNotFound();
            }
            ViewBag.passagiernummer = new SelectList(db.PassagierVoorVlucht, "passagiernummer", "stoel", @object.passagiernummer);
            return View(@object);
        }

        // POST: Objects/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "volgnummer,passagiernummer,vluchtnummer,gewicht")] gelre_airport.Models.Object @object)
        {
            if (ModelState.IsValid)
            {
                db.Entry(@object).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.passagiernummer = new SelectList(db.PassagierVoorVlucht, "passagiernummer", "stoel", @object.passagiernummer);
            return View(@object);
        }

        // GET: Objects/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            gelre_airport.Models.Object @object = db.Object.Find(id);
            if (@object == null)
            {
                return HttpNotFound();
            }
            return View(@object);
        }

        // POST: Objects/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            gelre_airport.Models.Object @object = db.Object.Find(id);
            db.Object.Remove(@object);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
