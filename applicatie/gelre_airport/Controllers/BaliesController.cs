﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using gelre_airport.Models;

namespace gelre_airport.Controllers
{
    public class BaliesController : Controller
    {
        private GelreAirportEntities db = new GelreAirportEntities();

        // GET: Balies
        public ActionResult Index()
        {
            return View(db.Balie.ToList());
        }

        // GET: Balies/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Balie balie = db.Balie.Find(id);
            if (balie == null)
            {
                return HttpNotFound();
            }
            return View(balie);
        }

        public ActionResult Select(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Balie balie = db.Balie.Find(id);
            Session["balie"] = balie;
            if (balie == null)
            {
                return HttpNotFound();
            }
            return View(balie);
        }

        // GET: Balies/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Balies/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "balienummer")] Balie balie)
        {
            if (ModelState.IsValid)
            {
                db.Balie.Add(balie);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(balie);
        }

        // GET: Balies/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Balie balie = db.Balie.Find(id);
            if (balie == null)
            {
                return HttpNotFound();
            }
            return View(balie);
        }

        // POST: Balies/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "balienummer")] Balie balie)
        {
            if (ModelState.IsValid)
            {
                db.Entry(balie).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(balie);
        }

        // GET: Balies/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Balie balie = db.Balie.Find(id);
            if (balie == null)
            {
                return HttpNotFound();
            }
            return View(balie);
        }

        // POST: Balies/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Balie balie = db.Balie.Find(id);
            db.Balie.Remove(balie);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
