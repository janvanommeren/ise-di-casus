﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using gelre_airport.Models;

namespace gelre_airport.Controllers
{
    public class PassagierVoorVluchtsController : Controller
    {
        private GelreAirportEntities db = new GelreAirportEntities();

        // GET: PassagierVoorVluchts
        public ActionResult Index()
        {
            var passagierVoorVlucht = db.PassagierVoorVlucht.Include(p => p.Balie).Include(p => p.Passagier).Include(p => p.Vlucht);
            return View(passagierVoorVlucht.ToList());
        }

        // GET: PassagierVoorVluchts/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PassagierVoorVlucht passagierVoorVlucht = db.PassagierVoorVlucht.Find(id);
            if (passagierVoorVlucht == null)
            {
                return HttpNotFound();
            }
            return View(passagierVoorVlucht);
        }

        // GET: PassagierVoorVluchts/Create
        public ActionResult Create()
        {
            ViewBag.balienummer = new SelectList(db.Balie, "balienummer", "balienummer");
            ViewBag.passagiernummer = new SelectList(db.Passagier, "passagiernummer", "naam");
            ViewBag.vluchtnummer = new SelectList(db.Vlucht, "vluchtnummer", "gatecode");
            return View();
        }

        // POST: PassagierVoorVluchts/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "passagiernummer,vluchtnummer,balienummer,inchecktijdstip,stoel")] PassagierVoorVlucht passagierVoorVlucht)
        {
            if (ModelState.IsValid)
            {
                db.PassagierVoorVlucht.Add(passagierVoorVlucht);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.balienummer = new SelectList(db.Balie, "balienummer", "balienummer", passagierVoorVlucht.balienummer);
            ViewBag.passagiernummer = new SelectList(db.Passagier, "passagiernummer", "naam", passagierVoorVlucht.passagiernummer);
            ViewBag.vluchtnummer = new SelectList(db.Vlucht, "vluchtnummer", "gatecode", passagierVoorVlucht.vluchtnummer);
            return View(passagierVoorVlucht);
        }

        // GET: PassagierVoorVluchts/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PassagierVoorVlucht passagierVoorVlucht = db.PassagierVoorVlucht.Find(id);
            if (passagierVoorVlucht == null)
            {
                return HttpNotFound();
            }
            ViewBag.balienummer = new SelectList(db.Balie, "balienummer", "balienummer", passagierVoorVlucht.balienummer);
            ViewBag.passagiernummer = new SelectList(db.Passagier, "passagiernummer", "naam", passagierVoorVlucht.passagiernummer);
            ViewBag.vluchtnummer = new SelectList(db.Vlucht, "vluchtnummer", "gatecode", passagierVoorVlucht.vluchtnummer);
            return View(passagierVoorVlucht);
        }

        // POST: PassagierVoorVluchts/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        public ActionResult Edit([Bind(Include = "passagiernummer,vluchtnummer,inchecktijdstip,stoel")] PassagierVoorVlucht passagierVoorVlucht)
        {
            Balie balie = Session["balie"] as Balie;
            if (ModelState.IsValid)
            {
                try
                {
                    passagierVoorVlucht.balienummer = balie.balienummer;
                    db.Entry(passagierVoorVlucht).State = EntityState.Modified;
                    db.SaveChanges();
                    return RedirectToAction("AllDetails", "Passagiers", new { id = passagierVoorVlucht.passagiernummer, vluchtnummer = passagierVoorVlucht.vluchtnummer });
                } catch (InvalidOperationException e) {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
            }
            ViewBag.balienummer = balie.balienummer;
            ViewBag.passagiernummer = new SelectList(db.Passagier, "passagiernummer", "naam", passagierVoorVlucht.passagiernummer);
            ViewBag.vluchtnummer = new SelectList(db.Vlucht, "vluchtnummer", "gatecode", passagierVoorVlucht.vluchtnummer);
            return View(passagierVoorVlucht);
        }

        // GET: PassagierVoorVluchts/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PassagierVoorVlucht passagierVoorVlucht = db.PassagierVoorVlucht.Find(id);
            if (passagierVoorVlucht == null)
            {
                return HttpNotFound();
            }
            return View(passagierVoorVlucht);
        }

        // POST: PassagierVoorVluchts/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            PassagierVoorVlucht passagierVoorVlucht = db.PassagierVoorVlucht.Find(id);
            db.PassagierVoorVlucht.Remove(passagierVoorVlucht);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
