﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using gelre_airport.Models;

namespace gelre_airport.Controllers
{
    public class VluchtenController : Controller
    {
        private GelreAirportEntities db = new GelreAirportEntities();

        // GET: Vluchten
        public ActionResult Index()
        {
            var vlucht = db.Vlucht.Include(v => v.Gate).Include(v => v.Luchthaven).Include(v => v.Maatschappij).Include(v => v.Vliegtuig);
            return View(vlucht.ToList());
        }

        // GET: Vluchten/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Vlucht vlucht = db.Vlucht.Find(id);
            if (vlucht == null)
            {
                return HttpNotFound();
            }
            return View(vlucht);
        }

        // GET: Vluchten/Create
        public ActionResult Create()
        {
            ViewBag.gatecode = new SelectList(db.Gate, "gatecode", "gatecode");
            ViewBag.luchthavencode = new SelectList(db.Luchthaven, "luchthavencode", "naam");
            ViewBag.maatschappijcode = new SelectList(db.Maatschappij, "maatschappijcode", "naam");
            ViewBag.vliegtuigtype = new SelectList(db.Vliegtuig, "vliegtuigtype", "vliegtuigtype");
            return View();
        }

        // POST: Vluchten/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "vluchtnummer,gatecode,maatschappijcode,luchthavencode,vliegtuigtype,max_aantal_psgrs,max_totaalgewicht,max_ppgewicht,vertrektijdstip,aankomsttijdstip")] Vlucht vlucht)
        {
            if (ModelState.IsValid)
            {
                db.Vlucht.Add(vlucht);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.gatecode = new SelectList(db.Gate, "gatecode", "gatecode", vlucht.gatecode);
            ViewBag.luchthavencode = new SelectList(db.Luchthaven, "luchthavencode", "naam", vlucht.luchthavencode);
            ViewBag.maatschappijcode = new SelectList(db.Maatschappij, "maatschappijcode", "naam", vlucht.maatschappijcode);
            ViewBag.vliegtuigtype = new SelectList(db.Vliegtuig, "vliegtuigtype", "vliegtuigtype", vlucht.vliegtuigtype);
            return View(vlucht);
        }

        // GET: Vluchten/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Vlucht vlucht = db.Vlucht.Find(id);
            if (vlucht == null)
            {
                return HttpNotFound();
            }
            ViewBag.gatecode = new SelectList(db.Gate, "gatecode", "gatecode", vlucht.gatecode);
            ViewBag.luchthavencode = new SelectList(db.Luchthaven, "luchthavencode", "naam", vlucht.luchthavencode);
            ViewBag.maatschappijcode = new SelectList(db.Maatschappij, "maatschappijcode", "naam", vlucht.maatschappijcode);
            ViewBag.vliegtuigtype = new SelectList(db.Vliegtuig, "vliegtuigtype", "vliegtuigtype", vlucht.vliegtuigtype);
            return View(vlucht);
        }

        // POST: Vluchten/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "vluchtnummer,gatecode,maatschappijcode,luchthavencode,vliegtuigtype,max_aantal_psgrs,max_totaalgewicht,max_ppgewicht,vertrektijdstip,aankomsttijdstip")] Vlucht vlucht)
        {
            if (ModelState.IsValid)
            {
                db.Entry(vlucht).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.gatecode = new SelectList(db.Gate, "gatecode", "gatecode", vlucht.gatecode);
            ViewBag.luchthavencode = new SelectList(db.Luchthaven, "luchthavencode", "naam", vlucht.luchthavencode);
            ViewBag.maatschappijcode = new SelectList(db.Maatschappij, "maatschappijcode", "naam", vlucht.maatschappijcode);
            ViewBag.vliegtuigtype = new SelectList(db.Vliegtuig, "vliegtuigtype", "vliegtuigtype", vlucht.vliegtuigtype);
            return View(vlucht);
        }

        // GET: Vluchten/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Vlucht vlucht = db.Vlucht.Find(id);
            if (vlucht == null)
            {
                return HttpNotFound();
            }
            return View(vlucht);
        }

        // POST: Vluchten/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Vlucht vlucht = db.Vlucht.Find(id);
            db.Vlucht.Remove(vlucht);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
