﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(gelre_airport.Startup))]
namespace gelre_airport
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
