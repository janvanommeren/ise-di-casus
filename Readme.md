# ISE Casus DI

## Mappen structuur
Hieronder staat de mappen structuur beschreven die wij hanteren binnen deze casus.
De geneste koppen vertegenwoordigt de mappenstructuur dus een kop 4 onder een kop 3 houdt in dat map 4 in map 3 staat.

### Database
De scripts voor de database worden geplaatst in de map database.
In de map database staan vervolgens mappen met de verschillende database onderdelen.
Per tabel dient er in de desbetreffende map een scriptje aangemaakt te worden met de betreffende functionaliteit.
Dus bijvoorbeeld in de map constraints zul je per tabel een scriptje vinden met de daarbij horende constraints.

#### Constraints

#### Create

#### Functions

#### Inserts
In deze map bevinden zich de insert scripts voor de testpopulatie, en de testdata voor alle constraints.

### Documenten
In deze map bevinden zich de documenten die bij de casus horen.
Hieronder vallen de geschreven opdrachten en de casus opdracht zelf.
Ook staat er informatie zoals een database diagram.